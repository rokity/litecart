-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Creato il: Nov 15, 2015 alle 22:16
-- Versione del server: 5.6.27-0ubuntu1
-- Versione PHP: 5.6.11-1ubuntu3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `litecart`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_addresses`
--

CREATE TABLE IF NOT EXISTS `lc_addresses` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `company` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `address1` varchar(64) NOT NULL,
  `address2` varchar(64) NOT NULL,
  `city` varchar(32) NOT NULL,
  `postcode` varchar(8) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_cart_items`
--

CREATE TABLE IF NOT EXISTS `lc_cart_items` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `cart_uid` varchar(13) NOT NULL,
  `key` varchar(32) NOT NULL,
  `product_id` int(11) NOT NULL,
  `options` varchar(2048) NOT NULL,
  `quantity` decimal(11,4) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_categories`
--

CREATE TABLE IF NOT EXISTS `lc_categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `code` varchar(64) NOT NULL,
  `list_style` varchar(32) NOT NULL,
  `dock` varchar(32) NOT NULL,
  `keywords` varchar(256) NOT NULL,
  `image` varchar(256) NOT NULL,
  `priority` tinyint(2) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_categories`
--

INSERT INTO `lc_categories` (`id`, `parent_id`, `status`, `code`, `list_style`, `dock`, `keywords`, `image`, `priority`, `date_updated`, `date_created`) VALUES
(1, 0, 1, '', 'columns', 'menu', '', '', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(2, 1, 1, '', 'rows', '', '', '', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_categories_info`
--

CREATE TABLE IF NOT EXISTS `lc_categories_info` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `short_description` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `head_title` varchar(128) NOT NULL,
  `h1_title` varchar(128) NOT NULL,
  `meta_description` varchar(256) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_categories_info`
--

INSERT INTO `lc_categories_info` (`id`, `category_id`, `language_code`, `name`, `short_description`, `description`, `head_title`, `h1_title`, `meta_description`) VALUES
(1, 1, 'en', 'Rubber Ducks', '', '', '', '', ''),
(2, 2, 'en', 'Subcategory', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_countries`
--

CREATE TABLE IF NOT EXISTS `lc_countries` (
  `id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(64) NOT NULL,
  `domestic_name` varchar(64) NOT NULL,
  `iso_code_1` varchar(3) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `tax_id_format` varchar(64) NOT NULL,
  `address_format` varchar(128) NOT NULL,
  `postcode_format` varchar(512) NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `phone_code` varchar(3) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=240 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_countries`
--

INSERT INTO `lc_countries` (`id`, `status`, `name`, `domestic_name`, `iso_code_1`, `iso_code_2`, `iso_code_3`, `tax_id_format`, `address_format`, `postcode_format`, `postcode_required`, `language_code`, `currency_code`, `phone_code`, `date_updated`, `date_created`) VALUES
(1, 1, 'Afghanistan', '', '004', 'AF', 'AFG', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'fa', 'AFN', '93', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(2, 1, 'Albania', '', '008', 'AL', 'ALB', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'sq', 'ALL', '355', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(3, 1, 'Algeria', '', '012', 'DZ', 'DZA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'ar', 'DZD', '213', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(4, 1, 'American Samoa', '', '016', 'AS', 'ASM', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '96799', 0, 'en', 'USD', '168', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(5, 1, 'Andorra', '', '020', 'AD', 'AND', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', 'AD\\d{3}', 0, 'ca', 'EUR', '376', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(6, 1, 'Angola', '', '024', 'AO', 'AGO', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'pt', 'AOA', '244', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(7, 1, 'Anguilla', '', '660', 'AI', 'AIA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'XCD', '126', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(8, 1, 'Antarctica', '', '010', 'AQ', 'ATA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, '', 'XCD', '672', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(9, 1, 'Antigua and Barbuda', '', '028', 'AG', 'ATG', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'XCD', '126', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(10, 1, 'Argentina', '', '032', 'AR', 'ARG', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '([A-HJ-NP-Z])?\\d{4}([A-Z]{3})?', 0, 'es', 'ARS', '54', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(11, 1, 'Armenia', '', '051', 'AM', 'ARM', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '(37)?\\d{4}', 0, 'hy', 'AMD', '374', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(12, 1, 'Aruba', '', '533', 'AW', 'ABW', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'nl', 'AWG', '297', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(13, 1, 'Australia', '', '036', 'AU', 'AUS', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'en', 'AUD', '61', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(14, 1, 'Austria', '', '040', 'AT', 'AUT', '^(AT)?(U[A-Zd]{8})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'de', 'EUR', '43', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(15, 1, 'Azerbaijan', '', '031', 'AZ', 'AZE', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'az', 'AZN', '994', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(16, 1, 'Bahamas', '', '044', 'BS', 'BHS', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'BSD', '124', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(17, 1, 'Bahrain', '', '048', 'BH', 'BHR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '((1[0-2]|[2-9])\\d{2})?', 0, 'ar', 'BHD', '973', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(18, 1, 'Bangladesh', '', '050', 'BD', 'BGD', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'bn', 'BDT', '880', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(19, 1, 'Barbados', '', '052', 'BB', 'BRB', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '(BB\\d{5})?', 0, 'en', 'BBD', '124', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(20, 1, 'Belarus', '', '112', 'BY', 'BLR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{6}', 0, 'be', 'BYR', '375', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(21, 1, 'Belgium', '', '056', 'BE', 'BEL', '^(BE)?(0)?(d{9})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'nl', 'EUR', '32', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(22, 1, 'Belize', '', '084', 'BZ', 'BLZ', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'BZD', '501', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(23, 1, 'Benin', '', '204', 'BJ', 'BEN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'fr', 'XOF', '229', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(24, 1, 'Bermuda', '', '060', 'BM', 'BMU', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '[A-Z]{2}[ ]?[A-Z0-9]{2}', 0, 'en', 'BMD', '144', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(25, 1, 'Bhutan', '', '064', 'BT', 'BTN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'dz', 'BTN', '975', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(26, 1, 'Bolivia', '', '068', 'BO', 'BOL', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'es', 'BOB', '591', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(27, 1, 'Bosnia and Herzegowina', '', '070', 'BA', 'BIH', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'bs', 'BAM', '387', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(28, 1, 'Botswana', '', '072', 'BW', 'BWA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'BWP', '267', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(29, 1, 'Bouvet Island', '', '074', 'BV', 'BVT', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, '', 'NOK', '47', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(30, 1, 'Brazil', '', '076', 'BR', 'BRA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}[\\-]?\\d{3}', 0, 'pt', 'BRL', '55', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(31, 1, 'British Indian Ocean Territory', '', '086', 'IO', 'IOT', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', 'BBND 1ZZ', 0, 'en', 'USD', '246', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(32, 1, 'Brunei Darussalam', '', '096', 'BN', 'BRN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '[A-Z]{2}[ ]?\\d{4}', 0, 'ms', 'BND', '673', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(33, 1, 'Bulgaria', '', '100', 'BG', 'BGR', '^(BG)?(d{9,10})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'bg', 'BGN', '359', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(34, 1, 'Burkina Faso', '', '854', 'BF', 'BFA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'fr', 'XOF', '226', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(35, 1, 'Burundi', '', '108', 'BI', 'BDI', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'fr', 'BIF', '257', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(36, 1, 'Cambodia', '', '116', 'KH', 'KHM', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'km', 'KHR', '855', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(37, 1, 'Cameroon', '', '120', 'CM', 'CMR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'XAF', '237', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(38, 1, 'Canada', '', '124', 'CA', 'CAN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '[ABCEGHJKLMNPRSTVXY]\\d[ABCEGHJ-NPRSTV-Z][ ]?\\d[ABCEGHJ-NPRSTV-Z]\\d', 0, 'en', 'CAD', '1', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(39, 1, 'Cape Verde', '', '132', 'CV', 'CPV', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'pt', 'CVE', '238', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(40, 1, 'Cayman Islands', '', '136', 'KY', 'CYM', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'KYD', '134', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(41, 1, 'Central African Republic', '', '140', 'CF', 'CAF', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'fr', 'XAF', '236', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(42, 1, 'Chad', '', '148', 'TD', 'TCD', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'fr', 'XAF', '235', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(43, 1, 'Chile', '', '152', 'CL', 'CHL', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'es', 'CLP', '56', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(44, 1, 'China', '', '156', 'CN', 'CHN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{6}', 0, 'zh', 'CNY', '86', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(45, 1, 'Christmas Island', '', '162', 'CX', 'CXR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '6798', 0, 'en', 'AUD', '61', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(46, 1, 'Cocos (Keeling) Islands', '', '166', 'CC', 'CCK', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '6799', 0, 'ms', 'AUD', '61', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(47, 1, 'Colombia', '', '170', 'CO', 'COL', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'es', 'COP', '57', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(48, 1, 'Comoros', '', '174', 'KM', 'COM', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'ar', 'KMF', '269', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(49, 1, 'Congo', '', '178', 'CG', 'COG', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'fr', 'XAF', '242', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(50, 1, 'Cook Islands', '', '184', 'CK', 'COK', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'en', 'NZD', '682', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(51, 1, 'Costa Rica', '', '188', 'CR', 'CRI', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4,5}|\\d{3}-\\d{4}', 0, 'es', 'CRC', '506', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(52, 1, 'Cote D''Ivoire', '', '384', 'CI', 'CIV', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'fr', 'XOF', '225', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(53, 1, 'Croatia', '', '191', 'HR', 'HRV', '^(HR)?(d{11})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'hr', 'HRK', '385', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(54, 1, 'Cuba', '', '192', 'CU', 'CUB', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'es', 'CUP', '53', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(55, 1, 'Cyprus', '', '196', 'CY', 'CYP', '^(CY)?(d{8}[A-Z]{1})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'el', 'EUR', '357', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(56, 1, 'Czech Republic', '', '203', 'CZ', 'CZE', '^(CZ)?(d{8,10})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{3}[ ]?\\d{2}', 0, 'cs', 'CZK', '420', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(57, 1, 'Denmark', '', '208', 'DK', 'DNK', '^(DK)?(d{8})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'da', 'DKK', '45', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(58, 1, 'Djibouti', '', '262', 'DJ', 'DJI', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'fr', 'DJF', '253', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(59, 1, 'Dominica', '', '212', 'DM', 'DMA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'XCD', '176', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(60, 1, 'Dominican Republic', '', '214', 'DO', 'DOM', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'es', 'DOP', '180', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(61, 1, 'East Timor', '', '626', 'TP', 'TMP', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, '', 'USD', '670', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(62, 1, 'Ecuador', '', '218', 'EC', 'ECU', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '([A-Z]\\d{4}[A-Z]|(?:[A-Z]{2})?\\d{6})?', 0, 'es', 'ECS', '593', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(63, 1, 'Egypt', '', '818', 'EG', 'EGY', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'ar', 'EGP', '20', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(64, 1, 'El Salvador', '', '222', 'SV', 'SLV', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'es', 'SVC', '503', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(65, 1, 'Equatorial Guinea', '', '226', 'GQ', 'GNQ', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'es', 'XAF', '240', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(66, 1, 'Eritrea', '', '232', 'ER', 'ERI', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'aa', 'ERN', '291', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(67, 1, 'Estonia', '', '233', 'EE', 'EST', '^(EE)?(d{9})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'et', 'EUR', '372', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(68, 1, 'Ethiopia', '', '231', 'ET', 'ETH', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'am', 'ETB', '251', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(69, 1, 'Falkland Islands (Malvinas)', '', '238', 'FK', 'FLK', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', 'FIQQ 1ZZ', 0, 'en', 'FKP', '500', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(70, 1, 'Faroe Islands', '', '234', 'FO', 'FRO', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{3}', 0, 'fo', 'DKK', '298', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(71, 1, 'Fiji', '', '242', 'FJ', 'FJI', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'FJD', '679', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(72, 1, 'Finland', '', '246', 'FI', 'FIN', '^(FI)?(d{8})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'fi', 'EUR', '358', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(73, 1, 'France', '', '250', 'FR', 'FRA', '^(FR)?(([A-Z]{2}|d{2})d{9})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{2}[ ]?\\d{3}', 0, 'fr', 'EUR', '33', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(75, 1, 'French Guiana', '', '254', 'GF', 'GUF', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '9[78]3\\d{2}', 0, 'fr', 'EUR', '594', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(76, 1, 'French Polynesia', '', '258', 'PF', 'PYF', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '987\\d{2}', 0, 'fr', 'XPF', '689', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(77, 1, 'French Southern Territories', '', '260', 'TF', 'ATF', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'fr', 'EUR', '262', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(78, 1, 'Gabon', '', '266', 'GA', 'GAB', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'fr', 'XAF', '241', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(79, 1, 'Gambia', '', '270', 'GM', 'GMB', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'GMD', '220', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(80, 1, 'Georgia', '', '268', 'GE', 'GEO', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'ka', 'GEL', '995', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(81, 1, 'Germany', '', '276', 'DE', 'DEU', '^(DE)?(d{9})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'de', 'EUR', '49', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(82, 1, 'Ghana', '', '288', 'GH', 'GHA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'GHS', '233', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(83, 1, 'Gibraltar', '', '292', 'GI', 'GIB', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'GIP', '350', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(84, 1, 'Greece', '', '300', 'GR', 'GRC', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{3}[ ]?\\d{2}', 0, 'el', 'EUR', '30', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(85, 1, 'Greenland', '', '304', 'GL', 'GRL', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '39\\d{2}', 0, 'kl', 'DKK', '299', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(86, 1, 'Grenada', '', '308', 'GD', 'GRD', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'XCD', '147', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(87, 1, 'Guadeloupe', '', '312', 'GP', 'GLP', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '9[78][01]\\d{2}', 0, 'fr', 'EUR', '590', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(88, 1, 'Guam', '', '316', 'GU', 'GUM', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '969[123]\\d([ \\-]\\d{4})?', 0, 'en', 'USD', '167', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(89, 1, 'Guatemala', '', '320', 'GT', 'GTM', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'es', 'QTQ', '502', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(90, 1, 'Guinea', '', '324', 'GN', 'GIN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{3}', 0, 'fr', 'GNF', '224', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(91, 1, 'Guinea-bissau', '', '624', 'GW', 'GNB', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'pt', 'GWP', '245', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(92, 1, 'Guyana', '', '328', 'GY', 'GUY', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'GYD', '592', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(93, 1, 'Haiti', '', '332', 'HT', 'HTI', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'ht', 'HTG', '509', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(94, 1, 'Heard and McDonald Islands', '', '334', 'HM', 'HMD', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, '', 'AUD', '0', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(95, 1, 'Honduras', '', '340', 'HN', 'HND', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '(?:\\d{5})?', 0, 'es', 'HNL', '504', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(96, 1, 'Hong Kong', '', '344', 'HK', 'HKG', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'zh', 'HKD', '852', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(97, 1, 'Hungary', '', '348', 'HU', 'HUN', '^(HU)?(d{8})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'hu', 'HUF', '36', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(98, 1, 'Iceland', '', '352', 'IS', 'ISL', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{3}', 0, 'is', 'ISK', '354', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(99, 1, 'India', '', '356', 'IN', 'IND', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{6}', 0, 'en', 'INR', '91', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(100, 1, 'Indonesia', '', '360', 'ID', 'IDN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'id', 'IDR', '62', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(101, 1, 'Iran, Islamic Republic of', '', '364', 'IR', 'IRN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'fa', 'IRR', '98', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(102, 1, 'Iraq', '', '368', 'IQ', 'IRQ', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'ar', 'IQD', '964', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(103, 1, 'Ireland', '', '372', 'IE', 'IRL', '^(IE)?(d{7}[A-Z]{2})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'EUR', '353', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(104, 1, 'Israel', '', '376', 'IL', 'ISR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'he', 'ILS', '972', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(105, 1, 'Italy', '', '380', 'IT', 'ITA', '^(IT)?(d{11})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'it', 'EUR', '39', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(106, 1, 'Jamaica', '', '388', 'JM', 'JAM', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'JMD', '187', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(107, 1, 'Japan', '', '392', 'JP', 'JPN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{3}-\\d{4}', 0, 'ja', 'JPY', '81', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(108, 1, 'Jordan', '', '400', 'JO', 'JOR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'ar', 'JOD', '962', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(109, 1, 'Kazakhstan', '', '398', 'KZ', 'KAZ', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{6}', 0, 'kk', 'KZT', '7', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(110, 1, 'Kenya', '', '404', 'KE', 'KEN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'en', 'KES', '254', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(111, 1, 'Kiribati', '', '296', 'KI', 'KIR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'AUD', '686', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(112, 1, 'North Korea', '', '408', 'KP', 'PRK', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'ko', 'KPW', '850', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(113, 1, 'Korea, Republic of', '', '410', 'KR', 'KOR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{3}[\\-]\\d{3}', 0, 'ko', 'KRW', '82', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(114, 1, 'Kuwait', '', '414', 'KW', 'KWT', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'ar', 'KWD', '965', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(115, 1, 'Kyrgyzstan', '', '417', 'KG', 'KGZ', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{6}', 0, 'ky', 'KGS', '996', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(116, 1, 'Lao People''s Democratic Republic', '', '418', 'LA', 'LAO', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'lo', 'LAK', '856', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(117, 1, 'Latvia', '', '428', 'LV', 'LVA', '^(LV)?(d{11})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'lv', 'LVL', '371', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(118, 1, 'Lebanon', '', '422', 'LB', 'LBN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '(\\d{4}([ ]?\\d{4})?)?', 0, 'ar', 'LBP', '961', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(119, 1, 'Lesotho', '', '426', 'LS', 'LSO', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{3}', 0, 'en', 'LSL', '266', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(120, 1, 'Liberia', '', '430', 'LR', 'LBR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'en', 'LRD', '231', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(121, 1, 'Libyan Arab Jamahiriya', '', '434', 'LY', 'LBY', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'ar', 'LYD', '218', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(122, 1, 'Liechtenstein', '', '438', 'LI', 'LIE', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '(948[5-9])|(949[0-7])', 0, 'de', 'CHF', '423', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(123, 1, 'Lithuania', '', '440', 'LT', 'LTU', '^(LT)?((d{9}|d{12}))$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'lt', 'LTL', '370', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(124, 1, 'Luxembourg', '', '442', 'LU', 'LUX', '^(LU)?(d{8})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'lb', 'EUR', '352', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(125, 1, 'Macau', '', '446', 'MO', 'MAC', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'zh', 'MOP', '853', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(126, 1, 'Macedonia', '', '807', 'MK', 'MKD', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'mk', 'MKD', '389', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(127, 1, 'Madagascar', '', '450', 'MG', 'MDG', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{3}', 0, 'fr', 'MGF', '261', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(128, 1, 'Malawi', '', '454', 'MW', 'MWI', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'ny', 'MWK', '265', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(129, 1, 'Malaysia', '', '458', 'MY', 'MYS', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'ms', 'MYR', '60', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(130, 1, 'Maldives', '', '462', 'MV', 'MDV', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'dv', 'MVR', '960', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(131, 1, 'Mali', '', '466', 'ML', 'MLI', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'fr', 'XOF', '223', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(132, 1, 'Malta', '', '470', 'MT', 'MLT', '^(MT)?(d{8})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'mt', 'EUR', '356', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(133, 1, 'Marshall Islands', '', '584', 'MH', 'MHL', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '969[67]\\d([ \\-]\\d{4})?', 0, 'mh', 'USD', '692', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(134, 1, 'Martinique', '', '474', 'MQ', 'MTQ', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '9[78]2\\d{2}', 0, 'fr', 'EUR', '596', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(135, 1, 'Mauritania', '', '478', 'MR', 'MRT', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'ar', 'MRO', '222', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(136, 1, 'Mauritius', '', '480', 'MU', 'MUS', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '(\\d{3}[A-Z]{2}\\d{3})?', 0, 'en', 'MUR', '230', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(137, 1, 'Mayotte', '', '175', 'YT', 'MYT', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '976\\d{2}', 0, 'fr', 'EUR', '262', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(138, 1, 'Mexico', '', '484', 'MX', 'MEX', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'es', 'MXN', '52', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(139, 1, 'Micronesia, Federated States of', '', '583', 'FM', 'FSM', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '(9694[1-4])([ \\-]\\d{4})?', 0, 'en', 'USD', '691', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(140, 1, 'Moldova, Republic of', '', '498', 'MD', 'MDA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'ro', 'MDL', '373', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(141, 1, 'Monaco', '', '492', 'MC', 'MCO', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '980\\d{2}', 0, 'fr', 'EUR', '377', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(142, 1, 'Mongolia', '', '496', 'MN', 'MNG', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{6}', 0, 'mn', 'MNT', '976', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(143, 1, 'Montserrat', '', '500', 'MS', 'MSR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'XCD', '166', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(144, 1, 'Morocco', '', '504', 'MA', 'MAR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'ar', 'MAD', '212', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(145, 1, 'Mozambique', '', '508', 'MZ', 'MOZ', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'pt', 'MZN', '258', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(146, 1, 'Myanmar', '', '104', 'MM', 'MMR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'my', 'MMK', '95', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(147, 1, 'Namibia', '', '516', 'NA', 'NAM', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'NAD', '264', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(148, 1, 'Nauru', '', '520', 'NR', 'NRU', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'na', 'AUD', '674', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(149, 1, 'Nepal', '', '524', 'NP', 'NPL', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'ne', 'NPR', '977', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(150, 1, 'Netherlands', '', '528', 'NL', 'NLD', '^(NL)?(d{9}Bd{2})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '[0-9]{4}', 0, 'nl', 'EUR', '31', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(151, 1, 'Netherlands Antilles', '', '530', 'AN', 'ANT', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'nl', 'ANG', '599', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(152, 1, 'New Caledonia', '', '540', 'NC', 'NCL', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '988\\d{2}', 0, 'fr', 'XPF', '687', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(153, 1, 'New Zealand', '', '554', 'NZ', 'NZL', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'en', 'NZD', '64', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(154, 1, 'Nicaragua', '', '558', 'NI', 'NIC', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '((\\d{4}-)?\\d{3}-\\d{3}(-\\d{1})?)?', 0, 'es', 'NIO', '505', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(155, 1, 'Niger', '', '562', 'NE', 'NER', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'fr', 'XOF', '227', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(156, 1, 'Nigeria', '', '566', 'NG', 'NGA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '(\\d{6})?', 0, 'en', 'NGN', '234', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(157, 1, 'Niue', '', '570', 'NU', 'NIU', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'NZD', '683', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(158, 1, 'Norfolk Island', '', '574', 'NF', 'NFK', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '2899', 0, 'en', 'AUD', '672', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(159, 1, 'Northern Mariana Islands', '', '580', 'MP', 'MNP', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '9695[012]([ \\-]\\d{4})?', 0, 'tl', 'USD', '167', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(160, 1, 'Norway', '', '578', 'NO', 'NOR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'no', 'NOK', '47', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(161, 1, 'Oman', '', '512', 'OM', 'OMN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '(PC )?\\d{3}', 0, 'ar', 'OMR', '968', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(162, 1, 'Pakistan', '', '586', 'PK', 'PAK', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'ur', 'PKR', '92', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(163, 1, 'Palau', '', '585', 'PW', 'PLW', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '96940', 0, 'en', 'USD', '680', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(164, 1, 'Panama', '', '591', 'PA', 'PAN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'es', 'PAB', '507', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(165, 1, 'Papua New Guinea', '', '598', 'PG', 'PNG', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{3}', 0, 'en', 'PGK', '675', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(166, 1, 'Paraguay', '', '600', 'PY', 'PRY', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'es', 'PYG', '595', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(167, 1, 'Peru', '', '604', 'PE', 'PER', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'es', 'PEN', '51', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(168, 1, 'Philippines', '', '608', 'PH', 'PHL', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'tl', 'PHP', '63', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(169, 1, 'Pitcairn', '', '612', 'PN', 'PCN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', 'PCRN 1ZZ', 0, 'en', 'NZD', '870', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(170, 1, 'Poland', '', '616', 'PL', 'POL', '^(PL)?(d{10})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{2}-\\d{3}', 0, 'pl', 'PLN', '48', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(171, 1, 'Portugal', '', '620', 'PT', 'PRT', '^(PT)?(d{9})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}([\\-]\\d{3})?', 0, 'pt', 'EUR', '351', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(172, 1, 'Puerto Rico', '', '630', 'PR', 'PRI', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '00[679]\\d{2}([ \\-]\\d{4})?', 0, 'en', 'USD', '1', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(173, 1, 'Qatar', '', '634', 'QA', 'QAT', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'ar', 'QAR', '974', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(174, 1, 'Reunion', '', '638', 'RE', 'REU', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '9[78]4\\d{2}', 0, 'fr', 'EUR', '262', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(175, 1, 'Romania', '', '642', 'RO', 'ROM', '^(RO)?(d{2,10})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{6}', 0, 'ro', 'RON', '40', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(176, 1, 'Russian Federation', '', '643', 'RU', 'RUS', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{6}', 0, 'ru', 'RUB', '7', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(177, 1, 'Rwanda', '', '646', 'RW', 'RWA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'rw', 'RWF', '250', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(178, 1, 'Saint Kitts and Nevis', '', '659', 'KN', 'KNA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'XCD', '186', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(179, 1, 'Saint Lucia', '', '662', 'LC', 'LCA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'XCD', '175', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(180, 1, 'Saint Vincent and the Grenadines', '', '670', 'VC', 'VCT', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'XCD', '178', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(181, 1, 'Samoa', '', '882', 'WS', 'WSM', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'sm', 'WST', '685', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(182, 1, 'San Marino', '', '674', 'SM', 'SMR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '4789\\d', 0, 'it', 'EUR', '378', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(183, 1, 'Sao Tome and Principe', '', '678', 'ST', 'STP', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'pt', 'STD', '239', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(184, 1, 'Saudi Arabia', '', '682', 'SA', 'SAU', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'ar', 'SAR', '966', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(185, 1, 'Senegal', '', '686', 'SN', 'SEN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'fr', 'XOF', '221', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(186, 1, 'Seychelles', '', '690', 'SC', 'SYC', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'SCR', '248', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(187, 1, 'Sierra Leone', '', '694', 'SL', 'SLE', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'SLL', '232', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(188, 1, 'Singapore', '', '702', 'SG', 'SGP', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{6}', 0, 'en', 'SGD', '65', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(189, 1, 'Slovak Republic', '', '703', 'SK', 'SVK', '^(SK)?(d{10})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{3}[ ]?\\d{2}', 0, 'sk', 'EUR', '421', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(190, 1, 'Slovenia', '', '705', 'SI', 'SVN', '^(SI)?(d{8})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'sl', 'EUR', '386', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(191, 1, 'Solomon Islands', '', '090', 'SB', 'SLB', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'SBD', '677', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(192, 1, 'Somalia', '', '706', 'SO', 'SOM', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'so', 'SOS', '252', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(193, 1, 'South Africa', '', '710', 'ZA', 'ZAF', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'zu', 'ZAR', '27', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(194, 1, 'South Georgia and South Sandwich Islands', '', '239', 'GS', 'SGS', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', 'SIQQ 1ZZ', 0, 'en', 'GBP', '500', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(195, 1, 'Spain', '', '724', 'ES', 'ESP', '^(ES)?([A-Z]d{7}[A-Z]|d{8}[A-Z]|[A-Z]d{8})$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'es', 'EUR', '34', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(196, 1, 'Sri Lanka', '', '144', 'LK', 'LKA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'si', 'LKR', '94', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(197, 1, 'St. Helena', '', '654', 'SH', 'SHN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '(ASCN|STHL) 1ZZ', 0, 'en', 'SHP', '290', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(198, 1, 'St. Pierre and Miquelon', '', '666', 'PM', 'SPM', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '9[78]5\\d{2}', 0, 'fr', 'EUR', '508', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(199, 1, 'Sudan', '', '729', 'SD', 'SDN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'ar', 'SDG', '249', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(200, 1, 'Suriname', '', '740', 'SR', 'SUR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'nl', 'SRD', '597', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(201, 1, 'Svalbard and Jan Mayen Islands', '', '744', 'SJ', 'SJM', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'no', 'NOK', '47', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(202, 1, 'Swaziland', '', '748', 'SZ', 'SWZ', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '[HLMS]\\d{3}', 0, 'en', 'SZL', '268', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(203, 1, 'Sweden', '', '752', 'SE', 'SWE', '^(SE)?(16|19|20)?([0-9]{6})(?:-)?([0-9]{4})?(01)?$', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{3}[ ]?\\d{2}', 0, 'sv', 'SEK', '46', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(204, 1, 'Switzerland', '', '756', 'CH', 'CHE', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'de', 'CHF', '41', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(205, 1, 'Syrian Arab Republic', '', '760', 'SY', 'SYR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'ar', 'SYP', '963', '2015-11-15 22:10:30', '2015-11-15 22:10:30');
INSERT INTO `lc_countries` (`id`, `status`, `name`, `domestic_name`, `iso_code_1`, `iso_code_2`, `iso_code_3`, `tax_id_format`, `address_format`, `postcode_format`, `postcode_required`, `language_code`, `currency_code`, `phone_code`, `date_updated`, `date_created`) VALUES
(206, 1, 'Taiwan', '', '158', 'TW', 'TWN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{3}(\\d{2})?', 0, 'zh', 'TWD', '886', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(207, 1, 'Tajikistan', '', '762', 'TJ', 'TJK', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{6}', 0, 'tg', 'TJS', '992', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(208, 1, 'Tanzania, United Republic of', '', '834', 'TZ', 'TZA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'sw', 'TZS', '255', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(209, 1, 'Thailand', '', '764', 'TH', 'THA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'th', 'THB', '66', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(210, 1, 'Togo', '', '768', 'TG', 'TGO', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'fr', 'XOF', '228', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(211, 1, 'Tokelau', '', '772', 'TK', 'TKL', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'NZD', '690', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(212, 1, 'Tonga', '', '776', 'TO', 'TON', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'to', 'TOP', '676', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(213, 1, 'Trinidad and Tobago', '', '780', 'TT', 'TTO', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'TTD', '186', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(214, 1, 'Tunisia', '', '788', 'TN', 'TUN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'ar', 'TND', '216', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(215, 1, 'Turkey', '', '792', 'TR', 'TUR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'tr', 'TRY', '90', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(216, 1, 'Turkmenistan', '', '795', 'TM', 'TKM', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{6}', 0, 'tk', 'TMT', '993', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(217, 1, 'Turks and Caicos Islands', '', '796', 'TC', 'TCA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', 'TKCA 1ZZ', 0, 'en', 'USD', '164', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(218, 1, 'Tuvalu', '', '798', 'TV', 'TUV', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'AUD', '688', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(219, 1, 'Uganda', '', '800', 'UG', 'UGA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'UGX', '256', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(220, 1, 'Ukraine', '', '804', 'UA', 'UKR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'uk', 'UAH', '380', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(221, 1, 'United Arab Emirates', '', '784', 'AE', 'ARE', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'ar', 'AED', '971', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(222, 1, 'United Kingdom', '', '826', 'GB', 'GBR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', 'GIR[ ]?0AA|((AB|AL|B|BA|BB|BD|BH|BL|BN|BR|BS|BT|CA|CB|CF|CH|CM|CO|CR|CT|CV|CW|DA|DD|DE|DG|DH|DL|DN|DT|DY|E|EC|EH|EN|EX|FK|FY|G|GL|GY|GU|HA|HD|HG|HP|HR|HS|HU|HX|IG|IM|IP|IV|JE|KA|KT|KW|KY|L|LA|LD|LE|LL|LN|LS|LU|M|ME|MK|ML|N|NE|NG|NN|NP|NR|NW|OL|OX|PA|PE|PH|PL|PO|PR|RG|RH|RM|S|SA|SE|SG|SK|SL|SM|SN|SO|SP|SR|SS|ST|SW|SY|TA|TD|TF|TN|TQ|TR|TS|TW|UB|W|WA|WC|WD|WF|WN|WR|WS|WV|YO|ZE)(\\d[\\dA-Z]?[ ]?\\d[ABD-HJLN-UW-Z]{2}))|BFPO[ ]?\\d{1,4}', 1, 'en', 'GBP', '44', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(223, 1, 'United States', '', '840', 'US', 'USA', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}([ \\-]\\d{4})?', 0, 'en', 'USD', '1', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(224, 1, 'United States Minor Outlying Islands', '', '581', 'UM', 'UMI', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'USD', '1', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(225, 1, 'Uruguay', '', '858', 'UY', 'URY', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'es', 'UYU', '598', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(226, 1, 'Uzbekistan', '', '860', 'UZ', 'UZB', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{6}', 0, 'uz', 'UZS', '998', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(227, 1, 'Vanuatu', '', '548', 'VU', 'VUT', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'bi', 'VUV', '678', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(228, 1, 'Vatican City State (Holy See)', '', '336', 'VA', 'VAT', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '00120', 0, 'la', 'EUR', '39', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(229, 1, 'Venezuela', '', '862', 'VE', 'VEN', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{4}', 0, 'es', 'VEF', '58', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(230, 1, 'Viet Nam', '', '704', 'VN', 'VNM', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'vi', 'VND', '84', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(231, 1, 'Virgin Islands (British)', '', '092', 'VG', 'VGB', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'USD', '128', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(232, 1, 'Virgin Islands (U.S.)', '', '850', 'VI', 'VIR', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '008(([0-4]\\d)|(5[01]))([ \\-]\\d{4})?', 0, 'en', 'USD', '134', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(233, 1, 'Wallis and Futuna Islands', '', '876', 'WF', 'WLF', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '986\\d{2}', 0, 'fr', 'XPF', '681', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(234, 1, 'Western Sahara', '', '732', 'EH', 'ESH', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'ar', 'MAD', '212', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(235, 1, 'Yemen', '', '887', 'YE', 'YEM', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'ar', 'YER', '967', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(236, 1, 'Yugoslavia', '', '890', 'YU', 'YUG', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, '', 'YUM', '381', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(237, 1, 'Democratic Republic of Congo', '', '180', 'CD', 'COD', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'fr', 'XAF', '243', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(238, 1, 'Zambia', '', '894', 'ZM', 'ZMB', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '\\d{5}', 0, 'en', 'ZMW', '260', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(239, 0, 'Zimbabwe', '', '716', 'ZW', 'ZWE', '', '%company\r\n%firstname %lastname\r\n%address1\r\n%address2\r\n%postcode %city\r\n%zone_name\r\n%country_name', '', 0, 'en', 'ZWD', '263', '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_currencies`
--

CREATE TABLE IF NOT EXISTS `lc_currencies` (
  `id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `code` varchar(3) NOT NULL,
  `number` varchar(3) NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` decimal(10,4) NOT NULL,
  `decimals` tinyint(1) NOT NULL,
  `prefix` varchar(8) NOT NULL,
  `suffix` varchar(8) NOT NULL,
  `priority` tinyint(2) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_currencies`
--

INSERT INTO `lc_currencies` (`id`, `status`, `code`, `number`, `name`, `value`, `decimals`, `prefix`, `suffix`, `priority`, `date_updated`, `date_created`) VALUES
(1, 1, 'USD', '840', 'US Dollars', 1.0000, 2, '$', '', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(2, 1, 'EUR', '978', 'Euros', 0.7260, 2, '', ' €', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_customers`
--

CREATE TABLE IF NOT EXISTS `lc_customers` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `tax_id` varchar(32) NOT NULL,
  `company` varchar(64) NOT NULL,
  `firstname` varchar(64) NOT NULL,
  `lastname` varchar(64) NOT NULL,
  `address1` varchar(64) NOT NULL,
  `address2` varchar(64) NOT NULL,
  `postcode` varchar(8) NOT NULL,
  `city` varchar(32) NOT NULL,
  `country_code` varchar(4) NOT NULL,
  `zone_code` varchar(8) NOT NULL,
  `phone` varchar(24) NOT NULL,
  `mobile` varchar(24) NOT NULL,
  `different_shipping_address` tinyint(1) NOT NULL,
  `shipping_company` varchar(64) NOT NULL,
  `shipping_firstname` varchar(64) NOT NULL,
  `shipping_lastname` varchar(64) NOT NULL,
  `shipping_address1` varchar(64) NOT NULL,
  `shipping_address2` varchar(64) NOT NULL,
  `shipping_city` varchar(32) NOT NULL,
  `shipping_postcode` varchar(8) NOT NULL,
  `shipping_country_code` varchar(4) NOT NULL,
  `shipping_zone_code` varchar(8) NOT NULL,
  `newsletter` tinyint(1) NOT NULL DEFAULT '1',
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_customers`
--

INSERT INTO `lc_customers` (`id`, `email`, `password`, `tax_id`, `company`, `firstname`, `lastname`, `address1`, `address2`, `postcode`, `city`, `country_code`, `zone_code`, `phone`, `mobile`, `different_shipping_address`, `shipping_company`, `shipping_firstname`, `shipping_lastname`, `shipping_address1`, `shipping_address2`, `shipping_city`, `shipping_postcode`, `shipping_country_code`, `shipping_zone_code`, `newsletter`, `date_updated`, `date_created`) VALUES
(1, 'user@email.com', '000000000000000000000000000000000000000000000000', '0000000000', 'ACME Corp.', 'John', 'Doe', 'Longway Street 1', '', 'XX1 X1', 'London', 'GB', '', '1-555-123-4567', '', 0, '', '', '', '', '', '', '', '', '', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_delivery_statuses`
--

CREATE TABLE IF NOT EXISTS `lc_delivery_statuses` (
  `id` int(11) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_delivery_statuses`
--

INSERT INTO `lc_delivery_statuses` (`id`, `date_updated`, `date_created`) VALUES
(1, '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_delivery_statuses_info`
--

CREATE TABLE IF NOT EXISTS `lc_delivery_statuses_info` (
  `id` int(11) NOT NULL,
  `delivery_status_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(256) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_delivery_statuses_info`
--

INSERT INTO `lc_delivery_statuses_info` (`id`, `delivery_status_id`, `language_code`, `name`, `description`) VALUES
(1, 1, 'en', '3-5 days', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_geo_zones`
--

CREATE TABLE IF NOT EXISTS `lc_geo_zones` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(256) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_geo_zones`
--

INSERT INTO `lc_geo_zones` (`id`, `name`, `description`, `date_updated`, `date_created`) VALUES
(1, 'European Union', 'All countries in the European Union', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(2, 'Canada', 'All states in Canada', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(3, 'United States of America', 'All states in USA', '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_languages`
--

CREATE TABLE IF NOT EXISTS `lc_languages` (
  `id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `code` varchar(2) NOT NULL,
  `code2` varchar(3) NOT NULL,
  `name` varchar(32) NOT NULL,
  `locale` varchar(32) NOT NULL,
  `charset` varchar(16) NOT NULL,
  `raw_date` varchar(32) NOT NULL,
  `raw_time` varchar(32) NOT NULL,
  `raw_datetime` varchar(32) NOT NULL,
  `format_date` varchar(32) NOT NULL,
  `format_time` varchar(32) NOT NULL,
  `format_datetime` varchar(32) NOT NULL,
  `decimal_point` varchar(1) NOT NULL,
  `thousands_sep` varchar(1) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `priority` tinyint(2) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_languages`
--

INSERT INTO `lc_languages` (`id`, `status`, `code`, `code2`, `name`, `locale`, `charset`, `raw_date`, `raw_time`, `raw_datetime`, `format_date`, `format_time`, `format_datetime`, `decimal_point`, `thousands_sep`, `currency_code`, `priority`, `date_updated`, `date_created`) VALUES
(1, 1, 'en', 'eng', 'English', 'en_US.utf8,en_US.UTF-8,english', 'UTF-8', 'm/d/y', 'h:i:s A', 'm/d/y h:i:s A', '%b %e %Y', '%I:%M %p', '%b %e %Y %I:%M %p', '.', ',', '', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_manufacturers`
--

CREATE TABLE IF NOT EXISTS `lc_manufacturers` (
  `id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  `keywords` varchar(256) NOT NULL,
  `image` varchar(256) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_manufacturers`
--

INSERT INTO `lc_manufacturers` (`id`, `status`, `code`, `name`, `keywords`, `image`, `date_updated`, `date_created`) VALUES
(1, 1, 'acme', 'ACME Corp.', '', 'manufacturers/1-acme-corp.png', '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_manufacturers_info`
--

CREATE TABLE IF NOT EXISTS `lc_manufacturers_info` (
  `id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `short_description` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `h1_title` varchar(128) NOT NULL,
  `head_title` varchar(128) NOT NULL,
  `meta_description` varchar(256) NOT NULL,
  `link` varchar(256) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_manufacturers_info`
--

INSERT INTO `lc_manufacturers_info` (`id`, `manufacturer_id`, `language_code`, `short_description`, `description`, `h1_title`, `head_title`, `meta_description`, `link`) VALUES
(1, 1, 'en', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_option_groups`
--

CREATE TABLE IF NOT EXISTS `lc_option_groups` (
  `id` int(11) NOT NULL,
  `function` varchar(32) NOT NULL,
  `required` tinyint(1) NOT NULL,
  `sort` varchar(32) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_option_groups`
--

INSERT INTO `lc_option_groups` (`id`, `function`, `required`, `sort`, `date_updated`, `date_created`) VALUES
(1, 'select', 1, 'priority', '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_option_groups_info`
--

CREATE TABLE IF NOT EXISTS `lc_option_groups_info` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(512) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_option_groups_info`
--

INSERT INTO `lc_option_groups_info` (`id`, `group_id`, `language_code`, `name`, `description`) VALUES
(1, 1, 'en', 'Size', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_option_values`
--

CREATE TABLE IF NOT EXISTS `lc_option_values` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `value` varchar(128) NOT NULL,
  `priority` tinyint(2) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_option_values`
--

INSERT INTO `lc_option_values` (`id`, `group_id`, `value`, `priority`) VALUES
(1, 1, '', 1),
(2, 1, '', 2),
(3, 1, '', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_option_values_info`
--

CREATE TABLE IF NOT EXISTS `lc_option_values_info` (
  `id` int(11) NOT NULL,
  `value_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_option_values_info`
--

INSERT INTO `lc_option_values_info` (`id`, `value_id`, `language_code`, `name`) VALUES
(1, 1, 'en', 'Small'),
(2, 2, 'en', 'Medium'),
(3, 3, 'en', 'Large');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_orders`
--

CREATE TABLE IF NOT EXISTS `lc_orders` (
  `id` int(11) NOT NULL,
  `uid` varchar(13) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_company` varchar(64) NOT NULL,
  `customer_firstname` varchar(64) NOT NULL,
  `customer_lastname` varchar(64) NOT NULL,
  `customer_email` varchar(128) NOT NULL,
  `customer_phone` varchar(24) NOT NULL,
  `customer_mobile` varchar(24) NOT NULL,
  `customer_tax_id` varchar(32) NOT NULL,
  `customer_address1` varchar(64) NOT NULL,
  `customer_address2` varchar(64) NOT NULL,
  `customer_city` varchar(32) NOT NULL,
  `customer_postcode` varchar(8) NOT NULL,
  `customer_country_code` varchar(2) NOT NULL,
  `customer_zone_code` varchar(8) NOT NULL,
  `shipping_company` varchar(64) NOT NULL,
  `shipping_firstname` varchar(64) NOT NULL,
  `shipping_lastname` varchar(64) NOT NULL,
  `shipping_address1` varchar(64) NOT NULL,
  `shipping_address2` varchar(64) NOT NULL,
  `shipping_city` varchar(32) NOT NULL,
  `shipping_postcode` varchar(8) NOT NULL,
  `shipping_country_code` varchar(2) NOT NULL,
  `shipping_zone_code` varchar(8) NOT NULL,
  `shipping_option_id` varchar(32) NOT NULL,
  `shipping_option_name` varchar(64) NOT NULL,
  `shipping_tracking_id` varchar(128) NOT NULL,
  `payment_option_id` varchar(32) NOT NULL,
  `payment_option_name` varchar(64) NOT NULL,
  `payment_transaction_id` varchar(128) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `weight_total` decimal(11,4) NOT NULL,
  `weight_class` varchar(2) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(11,4) NOT NULL,
  `payment_due` decimal(11,4) NOT NULL,
  `tax_total` decimal(11,4) NOT NULL,
  `client_ip` varchar(39) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_orders_comments`
--

CREATE TABLE IF NOT EXISTS `lc_orders_comments` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `hidden` int(11) NOT NULL,
  `text` varchar(512) NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_orders_items`
--

CREATE TABLE IF NOT EXISTS `lc_orders_items` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_stock_combination` varchar(32) NOT NULL,
  `options` varchar(512) NOT NULL,
  `name` varchar(128) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(11,4) NOT NULL,
  `tax` decimal(11,4) NOT NULL,
  `weight` decimal(11,4) NOT NULL,
  `weight_class` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_orders_totals`
--

CREATE TABLE IF NOT EXISTS `lc_orders_totals` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `module_id` varchar(32) NOT NULL,
  `title` varchar(128) NOT NULL,
  `value` float NOT NULL,
  `tax` float NOT NULL,
  `calculate` tinyint(1) NOT NULL,
  `priority` tinyint(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_order_statuses`
--

CREATE TABLE IF NOT EXISTS `lc_order_statuses` (
  `id` int(11) NOT NULL,
  `icon` varchar(24) NOT NULL,
  `color` varchar(7) NOT NULL,
  `is_sale` tinyint(1) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `priority` tinyint(2) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_order_statuses`
--

INSERT INTO `lc_order_statuses` (`id`, `icon`, `color`, `is_sale`, `notify`, `priority`, `date_updated`, `date_created`) VALUES
(1, 'fa-money', '#c0c0c0', 0, 0, 1, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(2, 'fa-clock-o', '#d7d96f', 1, 0, 2, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(3, 'fa-cog', '#ffa851', 1, 0, 3, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(4, 'fa-truck', '#99cc66', 1, 0, 4, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(5, 'fa-times', '#ff6666', 0, 0, 5, '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_order_statuses_info`
--

CREATE TABLE IF NOT EXISTS `lc_order_statuses_info` (
  `id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(256) NOT NULL,
  `email_message` varchar(2048) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_order_statuses_info`
--

INSERT INTO `lc_order_statuses_info` (`id`, `order_status_id`, `language_code`, `name`, `description`, `email_message`) VALUES
(1, 1, 'en', 'Awaiting payment', '', ''),
(2, 2, 'en', 'Pending', '', ''),
(3, 3, 'en', 'Processing', '', ''),
(4, 4, 'en', 'Dispatched', '', ''),
(5, 5, 'en', 'Cancelled', '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_pages`
--

CREATE TABLE IF NOT EXISTS `lc_pages` (
  `id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `dock` varchar(64) NOT NULL,
  `priority` tinyint(2) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_pages`
--

INSERT INTO `lc_pages` (`id`, `status`, `dock`, `priority`, `date_updated`, `date_created`) VALUES
(1, 1, 'customer_service,information', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(2, 1, 'customer_service,information', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(3, 1, 'customer_service,information', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(4, 1, 'customer_service,information', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_pages_info`
--

CREATE TABLE IF NOT EXISTS `lc_pages_info` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(256) NOT NULL,
  `content` text NOT NULL,
  `head_title` varchar(128) NOT NULL,
  `meta_description` varchar(256) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_pages_info`
--

INSERT INTO `lc_pages_info` (`id`, `page_id`, `language_code`, `title`, `content`, `head_title`, `meta_description`) VALUES
(1, 1, 'en', 'About Us', '<h1>About Us</h1><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fermentum quam eget molestie lacinia. Suspendisse consectetur velit vitae tellus commodo pharetra. Curabitur lobortis turpis tortor, id blandit metus pellentesque sit amet. Etiam cursus dolor purus, sit amet vestibulum ipsum aliquet nec. Nunc sed aliquet eros. Sed at vehicula urna. Aliquam euismod nisl a felis adipiscing tincidunt. Etiam vestibulum arcu sed massa ornare, vitae venenatis odio convallis.\r\n</p>\r\n \r\n<h2>\r\n	Subheading 2\r\n</h2>\r\n \r\n<p>\r\n	 Aliquam eget suscipit urna. Fusce sed lorem enim. Praesent dictum sagittis tellus, vel imperdiet urna tristique eu. Morbi sed orci eu odio varius tempor consequat ut lectus. Aliquam sagittis sapien vitae nulla porta adipiscing. Nullam pulvinar interdum malesuada. Ut blandit ligula quam, id luctus risus ultrices eget. Donec mattis turpis vel purus hendrerit, id ornare dui viverra. Donec at aliquet purus. Maecenas ut commodo lorem. Vivamus ornare sem eu convallis ullamcorper. \r\n</p>\r\n \r\n<h3>\r\n	Subheading 3\r\n</h3>\r\n \r\n<p>\r\n	 In in massa accumsan augue accumsan facilisis non eget dui. Ut volutpat nisl urna, ac dapibus ipsum fermentum iaculis. Donec sed lorem metus. Donec gravida et risus et consectetur. Proin aliquet, ipsum in faucibus condimentum, orci sapien sollicitudin mi, vitae molestie nunc odio vitae libero. Nullam pretium velit in sem sagittis, et facilisis mi fermentum. Aenean varius sed est et tincidunt. Praesent non imperdiet ligula. \r\n</p>', '', ''),
(2, 2, 'en', 'Delivery Information', '<h1>Delivery Information</h1><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fermentum quam eget molestie lacinia. Suspendisse consectetur velit vitae tellus commodo pharetra. Curabitur lobortis turpis tortor, id blandit metus pellentesque sit amet. Etiam cursus dolor purus, sit amet vestibulum ipsum aliquet nec. Nunc sed aliquet eros. Sed at vehicula urna. Aliquam euismod nisl a felis adipiscing tincidunt. Etiam vestibulum arcu sed massa ornare, vitae venenatis odio convallis.\r\n</p>\r\n \r\n<h2>\r\n	 Subheading 2 \r\n</h2>\r\n \r\n<p>\r\n	 Aliquam eget suscipit urna. Fusce sed lorem enim. Praesent dictum sagittis tellus, vel imperdiet urna tristique eu. Morbi sed orci eu odio varius tempor consequat ut lectus. Aliquam sagittis sapien vitae nulla porta adipiscing. Nullam pulvinar interdum malesuada. Ut blandit ligula quam, id luctus risus ultrices eget. Donec mattis turpis vel purus hendrerit, id ornare dui viverra. Donec at aliquet purus. Maecenas ut commodo lorem. Vivamus ornare sem eu convallis ullamcorper. \r\n</p>\r\n \r\n<h3>\r\n	 Subheading 3 \r\n</h3>\r\n \r\n<p>\r\n	 In in massa accumsan augue accumsan facilisis non eget dui. Ut volutpat nisl urna, ac dapibus ipsum fermentum iaculis. Donec sed lorem metus. Donec gravida et risus et consectetur. Proin aliquet, ipsum in faucibus condimentum, orci sapien sollicitudin mi, vitae molestie nunc odio vitae libero. Nullam pretium velit in sem sagittis, et facilisis mi fermentum. Aenean varius sed est et tincidunt. Praesent non imperdiet ligula. \r\n</p>', '', ''),
(3, 3, 'en', 'Privacy Policy', '<h1>Privacy Policy</h1><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fermentum quam eget molestie lacinia. Suspendisse consectetur velit vitae tellus commodo pharetra. Curabitur lobortis turpis tortor, id blandit metus pellentesque sit amet. Etiam cursus dolor purus, sit amet vestibulum ipsum aliquet nec. Nunc sed aliquet eros. Sed at vehicula urna. Aliquam euismod nisl a felis adipiscing tincidunt. Etiam vestibulum arcu sed massa ornare, vitae venenatis odio convallis.\r\n</p>\r\n \r\n<h2>\r\n	 Subheading 2 \r\n</h2>\r\n \r\n<p>\r\n	 Aliquam eget suscipit urna. Fusce sed lorem enim. Praesent dictum sagittis tellus, vel imperdiet urna tristique eu. Morbi sed orci eu odio varius tempor consequat ut lectus. Aliquam sagittis sapien vitae nulla porta adipiscing. Nullam pulvinar interdum malesuada. Ut blandit ligula quam, id luctus risus ultrices eget. Donec mattis turpis vel purus hendrerit, id ornare dui viverra. Donec at aliquet purus. Maecenas ut commodo lorem. Vivamus ornare sem eu convallis ullamcorper. \r\n</p>\r\n \r\n<h3>\r\n	 Subheading 3 \r\n</h3>\r\n \r\n<p>\r\n	 In in massa accumsan augue accumsan facilisis non eget dui. Ut volutpat nisl urna, ac dapibus ipsum fermentum iaculis. Donec sed lorem metus. Donec gravida et risus et consectetur. Proin aliquet, ipsum in faucibus condimentum, orci sapien sollicitudin mi, vitae molestie nunc odio vitae libero. Nullam pretium velit in sem sagittis, et facilisis mi fermentum. Aenean varius sed est et tincidunt. Praesent non imperdiet ligula. \r\n</p>', '', ''),
(4, 4, 'en', 'Terms & Conditions', '<h1>Terms &amp; Conditions</h1><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fermentum quam eget molestie lacinia. Suspendisse consectetur velit vitae tellus commodo pharetra. Curabitur lobortis turpis tortor, id blandit metus pellentesque sit amet. Etiam cursus dolor purus, sit amet vestibulum ipsum aliquet nec. Nunc sed aliquet eros. Sed at vehicula urna. Aliquam euismod nisl a felis adipiscing tincidunt. Etiam vestibulum arcu sed massa ornare, vitae venenatis odio convallis.\r\n</p>\r\n \r\n<h2>\r\n	 Subheading 2 \r\n</h2>\r\n \r\n<p>\r\n	 Aliquam eget suscipit urna. Fusce sed lorem enim. Praesent dictum sagittis tellus, vel imperdiet urna tristique eu. Morbi sed orci eu odio varius tempor consequat ut lectus. Aliquam sagittis sapien vitae nulla porta adipiscing. Nullam pulvinar interdum malesuada. Ut blandit ligula quam, id luctus risus ultrices eget. Donec mattis turpis vel purus hendrerit, id ornare dui viverra. Donec at aliquet purus. Maecenas ut commodo lorem. Vivamus ornare sem eu convallis ullamcorper. \r\n</p>\r\n \r\n<h3>\r\n	 Subheading 3 \r\n</h3>\r\n \r\n<p>\r\n	 In in massa accumsan augue accumsan facilisis non eget dui. Ut volutpat nisl urna, ac dapibus ipsum fermentum iaculis. Donec sed lorem metus. Donec gravida et risus et consectetur. Proin aliquet, ipsum in faucibus condimentum, orci sapien sollicitudin mi, vitae molestie nunc odio vitae libero. Nullam pretium velit in sem sagittis, et facilisis mi fermentum. Aenean varius sed est et tincidunt. Praesent non imperdiet ligula. \r\n</p>', '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_products`
--

CREATE TABLE IF NOT EXISTS `lc_products` (
  `id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `delivery_status_id` int(11) NOT NULL,
  `sold_out_status_id` int(11) NOT NULL,
  `default_category_id` int(11) NOT NULL,
  `product_groups` varchar(128) NOT NULL,
  `keywords` varchar(256) NOT NULL,
  `code` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `upc` varchar(24) NOT NULL COMMENT 'Deprecated',
  `gtin` varchar(32) NOT NULL,
  `taric` varchar(16) NOT NULL,
  `quantity` decimal(11,4) NOT NULL,
  `quantity_unit_id` int(1) NOT NULL,
  `weight` decimal(10,4) NOT NULL,
  `weight_class` varchar(2) NOT NULL,
  `dim_x` decimal(10,4) NOT NULL,
  `dim_y` decimal(10,4) NOT NULL,
  `dim_z` decimal(10,4) NOT NULL,
  `dim_class` varchar(2) NOT NULL,
  `purchase_price` decimal(10,4) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `image` varchar(256) NOT NULL,
  `views` int(11) NOT NULL,
  `purchases` int(11) NOT NULL,
  `date_valid_from` date NOT NULL,
  `date_valid_to` date NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_products`
--

INSERT INTO `lc_products` (`id`, `status`, `manufacturer_id`, `supplier_id`, `delivery_status_id`, `sold_out_status_id`, `default_category_id`, `product_groups`, `keywords`, `code`, `sku`, `upc`, `gtin`, `taric`, `quantity`, `quantity_unit_id`, `weight`, `weight_class`, `dim_x`, `dim_y`, `dim_z`, `dim_class`, `purchase_price`, `tax_class_id`, `image`, `views`, `purchases`, `date_valid_from`, `date_valid_to`, `date_updated`, `date_created`) VALUES
(1, 1, 1, 0, 1, 2, 2, '', '', 'rd001', 'RD001', '', '', '', 30.0000, 1, 0.5000, 'kg', 6.0000, 10.0000, 10.0000, 'cm', 10.0000, 1, 'products/1-yellow-duck-1.png', 1, 0, '0000-00-00', '0000-00-00', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(2, 1, 1, 0, 1, 2, 2, '', '', 'rd002', 'RD002', '', '', '', 30.0000, 1, 0.5000, 'kg', 6.0000, 10.0000, 10.0000, 'cm', 10.0000, 1, 'products/2-green-duck-1.png', 1, 0, '0000-00-00', '0000-00-00', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(3, 1, 1, 0, 1, 2, 1, '', '', 'rd003', 'RD003', '', '', '', 30.0000, 1, 0.5000, 'kg', 6.0000, 10.0000, 10.0000, 'cm', 10.0000, 1, 'products/3-red-duck-1.png', 1, 0, '0000-00-00', '0000-00-00', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(4, 1, 1, 0, 1, 2, 1, '', '', 'rd004', 'RD004', '', '', '', 30.0000, 1, 0.5000, 'kg', 6.0000, 10.0000, 10.0000, 'cm', 10.0000, 1, 'products/4-blue-duck-1.png', 1, 0, '0000-00-00', '0000-00-00', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(5, 1, 1, 0, 1, 2, 1, '', '', 'rd005', 'RD005', '', '', '', 30.0000, 1, 0.5000, 'kg', 6.0000, 10.0000, 10.0000, 'cm', 10.0000, 1, 'products/5-purple-duck-1.png', 1, 0, '0000-00-00', '0000-00-00', '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_products_campaigns`
--

CREATE TABLE IF NOT EXISTS `lc_products_campaigns` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `USD` decimal(11,4) NOT NULL,
  `EUR` decimal(11,4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_products_campaigns`
--

INSERT INTO `lc_products_campaigns` (`id`, `product_id`, `start_date`, `end_date`, `USD`, `EUR`) VALUES
(1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18.0000, 0.0000);

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_products_images`
--

CREATE TABLE IF NOT EXISTS `lc_products_images` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `filename` varchar(256) NOT NULL,
  `priority` tinyint(2) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_products_images`
--

INSERT INTO `lc_products_images` (`id`, `product_id`, `filename`, `priority`) VALUES
(1, 1, 'products/1-yellow-duck-1.png', 1),
(2, 2, 'products/2-green-duck-1.png', 1),
(3, 3, 'products/3-red-duck-1.png', 1),
(4, 4, 'products/4-blue-duck-1.png', 1),
(5, 5, 'products/5-purple-duck-1.png', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_products_info`
--

CREATE TABLE IF NOT EXISTS `lc_products_info` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `short_description` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `head_title` varchar(128) NOT NULL,
  `meta_description` varchar(256) NOT NULL,
  `attributes` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_products_info`
--

INSERT INTO `lc_products_info` (`id`, `product_id`, `language_code`, `name`, `short_description`, `description`, `head_title`, `meta_description`, `attributes`) VALUES
(1, 1, 'en', 'Yellow Duck', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sollicitudin ante massa, eget ornare libero porta congue.', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sollicitudin ante massa, eget ornare libero porta congue. Cras scelerisque dui non consequat sollicitudin. Sed pretium tortor ac auctor molestie. Nulla facilisi. Maecenas pulvinar nibh vitae lectus vehicula semper. Donec et aliquet velit. Curabitur non ullamcorper mauris. In hac habitasse platea dictumst. Phasellus ut pretium justo, sit amet bibendum urna. Maecenas sit amet arcu pulvinar, facilisis quam at, viverra nisi. Morbi sit amet adipiscing ante. Integer imperdiet volutpat ante, sed venenatis urna volutpat a. Proin justo massa, convallis vitae consectetur sit amet, facilisis id libero. \r\n</p>', '', '', 'Colors\r\nBody: Yellow\r\nEyes: Black\r\nBeak: Orange\r\n\r\nOther\r\nMaterial: Plastic'),
(2, 2, 'en', 'Green Duck', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sollicitudin ante massa, eget ornare libero porta congue.', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sollicitudin ante massa, eget ornare libero porta congue. Cras scelerisque dui non consequat sollicitudin. Sed pretium tortor ac auctor molestie. Nulla facilisi. Maecenas pulvinar nibh vitae lectus vehicula semper. Donec et aliquet velit. Curabitur non ullamcorper mauris. In hac habitasse platea dictumst. Phasellus ut pretium justo, sit amet bibendum urna. Maecenas sit amet arcu pulvinar, facilisis quam at, viverra nisi. Morbi sit amet adipiscing ante. Integer imperdiet volutpat ante, sed venenatis urna volutpat a. Proin justo massa, convallis vitae consectetur sit amet, facilisis id libero. \r\n</p>', '', '', ''),
(3, 3, 'en', 'Red Duck', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sollicitudin ante massa, eget ornare libero porta congue.', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sollicitudin ante massa, eget ornare libero porta congue. Cras scelerisque dui non consequat sollicitudin. Sed pretium tortor ac auctor molestie. Nulla facilisi. Maecenas pulvinar nibh vitae lectus vehicula semper. Donec et aliquet velit. Curabitur non ullamcorper mauris. In hac habitasse platea dictumst. Phasellus ut pretium justo, sit amet bibendum urna. Maecenas sit amet arcu pulvinar, facilisis quam at, viverra nisi. Morbi sit amet adipiscing ante. Integer imperdiet volutpat ante, sed venenatis urna volutpat a. Proin justo massa, convallis vitae consectetur sit amet, facilisis id libero. \r\n</p>', '', '', ''),
(4, 4, 'en', 'Blue Duck', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sollicitudin ante massa, eget ornare libero porta congue.', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sollicitudin ante massa, eget ornare libero porta congue. Cras scelerisque dui non consequat sollicitudin. Sed pretium tortor ac auctor molestie. Nulla facilisi. Maecenas pulvinar nibh vitae lectus vehicula semper. Donec et aliquet velit. Curabitur non ullamcorper mauris. In hac habitasse platea dictumst. Phasellus ut pretium justo, sit amet bibendum urna. Maecenas sit amet arcu pulvinar, facilisis quam at, viverra nisi. Morbi sit amet adipiscing ante. Integer imperdiet volutpat ante, sed venenatis urna volutpat a. Proin justo massa, convallis vitae consectetur sit amet, facilisis id libero. \r\n</p>', '', '', ''),
(5, 5, 'en', 'Purple Duck', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sollicitudin ante massa, eget ornare libero porta congue.', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sollicitudin ante massa, eget ornare libero porta congue. Cras scelerisque dui non consequat sollicitudin. Sed pretium tortor ac auctor molestie. Nulla facilisi. Maecenas pulvinar nibh vitae lectus vehicula semper. Donec et aliquet velit. Curabitur non ullamcorper mauris. In hac habitasse platea dictumst. Phasellus ut pretium justo, sit amet bibendum urna. Maecenas sit amet arcu pulvinar, facilisis quam at, viverra nisi. Morbi sit amet adipiscing ante. Integer imperdiet volutpat ante, sed venenatis urna volutpat a. Proin justo massa, convallis vitae consectetur sit amet, facilisis id libero. \r\n</p>', '', '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_products_options`
--

CREATE TABLE IF NOT EXISTS `lc_products_options` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `value_id` int(11) NOT NULL,
  `price_operator` varchar(1) NOT NULL,
  `USD` decimal(11,4) NOT NULL,
  `EUR` decimal(11,4) NOT NULL,
  `priority` tinyint(2) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_products_options`
--

INSERT INTO `lc_products_options` (`id`, `product_id`, `group_id`, `value_id`, `price_operator`, `USD`, `EUR`, `priority`, `date_updated`, `date_created`) VALUES
(1, 1, 1, 1, '+', 0.0000, 0.0000, 1, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(2, 1, 1, 2, '+', 2.5000, 0.0000, 2, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(3, 1, 1, 3, '+', 5.0000, 0.0000, 3, '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_products_options_stock`
--

CREATE TABLE IF NOT EXISTS `lc_products_options_stock` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `combination` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `weight` decimal(11,4) NOT NULL,
  `weight_class` varchar(2) NOT NULL,
  `dim_x` decimal(11,4) NOT NULL,
  `dim_y` decimal(11,4) NOT NULL,
  `dim_z` decimal(11,4) NOT NULL,
  `dim_class` varchar(2) NOT NULL,
  `quantity` decimal(11,4) NOT NULL,
  `priority` tinyint(2) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_products_options_stock`
--

INSERT INTO `lc_products_options_stock` (`id`, `product_id`, `combination`, `sku`, `weight`, `weight_class`, `dim_x`, `dim_y`, `dim_z`, `dim_class`, `quantity`, `priority`, `date_updated`, `date_created`) VALUES
(1, 1, '1-1', 'RD001-S', 1.0000, 'kg', 6.0000, 10.0000, 10.0000, 'cm', 10.0000, 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(2, 1, '1-2', 'RD001-M', 1.1000, 'kg', 8.0000, 12.5000, 12.5000, 'cm', 10.0000, 1, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(3, 1, '1-3', 'RD001-L', 1.2000, 'kg', 10.0000, 15.0000, 15.0000, 'cm', 10.0000, 2, '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_products_prices`
--

CREATE TABLE IF NOT EXISTS `lc_products_prices` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `USD` decimal(11,4) NOT NULL,
  `EUR` decimal(11,4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_products_prices`
--

INSERT INTO `lc_products_prices` (`id`, `product_id`, `USD`, `EUR`) VALUES
(1, 1, 20.0000, 0.0000),
(2, 2, 20.0000, 0.0000),
(3, 3, 20.0000, 0.0000),
(4, 4, 20.0000, 0.0000);

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_products_to_categories`
--

CREATE TABLE IF NOT EXISTS `lc_products_to_categories` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_products_to_categories`
--

INSERT INTO `lc_products_to_categories` (`product_id`, `category_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(4, 1),
(5, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_product_groups`
--

CREATE TABLE IF NOT EXISTS `lc_product_groups` (
  `id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_product_groups`
--

INSERT INTO `lc_product_groups` (`id`, `status`, `date_updated`, `date_created`) VALUES
(1, 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_product_groups_info`
--

CREATE TABLE IF NOT EXISTS `lc_product_groups_info` (
  `id` int(11) NOT NULL,
  `product_group_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_product_groups_info`
--

INSERT INTO `lc_product_groups_info` (`id`, `product_group_id`, `language_code`, `name`) VALUES
(1, 1, 'en', 'Gender');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_product_groups_values`
--

CREATE TABLE IF NOT EXISTS `lc_product_groups_values` (
  `id` int(11) NOT NULL,
  `product_group_id` int(11) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_product_groups_values`
--

INSERT INTO `lc_product_groups_values` (`id`, `product_group_id`, `date_updated`, `date_created`) VALUES
(1, 1, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(2, 1, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(3, 1, '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_product_groups_values_info`
--

CREATE TABLE IF NOT EXISTS `lc_product_groups_values_info` (
  `id` int(11) NOT NULL,
  `product_group_value_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_product_groups_values_info`
--

INSERT INTO `lc_product_groups_values_info` (`id`, `product_group_value_id`, `language_code`, `name`) VALUES
(1, 1, 'en', 'Male'),
(2, 2, 'en', 'Female'),
(3, 3, 'en', 'Unisex');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_quantity_units`
--

CREATE TABLE IF NOT EXISTS `lc_quantity_units` (
  `id` int(11) NOT NULL,
  `decimals` tinyint(1) NOT NULL,
  `separate` tinyint(1) NOT NULL,
  `priority` tinyint(2) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_quantity_units`
--

INSERT INTO `lc_quantity_units` (`id`, `decimals`, `separate`, `priority`, `date_updated`, `date_created`) VALUES
(1, 0, 0, 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_quantity_units_info`
--

CREATE TABLE IF NOT EXISTS `lc_quantity_units_info` (
  `id` int(11) NOT NULL,
  `quantity_unit_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(512) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_quantity_units_info`
--

INSERT INTO `lc_quantity_units_info` (`id`, `quantity_unit_id`, `language_code`, `name`, `description`) VALUES
(1, 1, 'en', 'pcs', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_settings`
--

CREATE TABLE IF NOT EXISTS `lc_settings` (
  `id` int(11) NOT NULL,
  `setting_group_key` varchar(64) NOT NULL,
  `type` enum('global','local') NOT NULL DEFAULT 'local',
  `title` varchar(128) NOT NULL,
  `description` varchar(512) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` varchar(2048) NOT NULL,
  `function` varchar(128) NOT NULL,
  `priority` tinyint(2) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_settings`
--

INSERT INTO `lc_settings` (`id`, `setting_group_key`, `type`, `title`, `description`, `key`, `value`, `function`, `priority`, `date_updated`, `date_created`) VALUES
(1, 'store_info', 'global', 'Store Name', 'The name of your store.', 'store_name', 'My Store', 'input()', 10, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(2, 'store_info', 'global', 'Store Email', 'The store e-mail address.', 'store_email', 'store@email.com', 'input()', 11, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(3, 'store_info', 'local', 'Store Tax ID', 'The store tax ID or VATIN.', 'store_tax_id', 'XX000000000000', 'input()', 12, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(4, 'store_info', 'local', 'Store Postal Address', 'The store postal address.', 'store_postal_address', 'My Store\r\nStreet\r\nPostcode City\r\nCountry', 'bigtext()', 13, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(5, 'store_info', 'local', 'Store Visiting Address', 'The store visiting address if applicable.', 'store_visiting_address', '', 'bigtext()', 14, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(6, 'store_info', 'local', 'Store Phone Number', 'The store phone number.', 'store_phone', '+1-212-555-DUCK', 'input()', 15, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(7, 'store_info', 'global', 'Store Country', 'The country of your store.', 'store_country_code', 'IT', 'countries()', 16, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(8, 'store_info', 'global', 'Store Time Zone', 'The store time zone.', 'store_timezone', 'Europe/Rome', 'timezones()', 17, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(9, 'store_info', 'local', 'Store Language', 'The spoken language of your organization.', 'store_language_code', 'en', 'languages()', 18, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(10, 'store_info', 'global', 'Store Currency', 'The currency of which all prices conform to.', 'store_currency_code', 'USD', 'currencies()', 19, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(11, 'store_info', 'global', 'Store Zone', 'The zone of your store.', 'store_zone_code', '', 'zones()', 20, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(12, 'store_info', 'global', 'Store Weight Class', 'The prefered weight class.', 'store_weight_class', 'kg', 'weight_classes()', 21, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(13, 'store_info', 'global', 'Store Length Class', 'The prefered length class.', 'store_length_class', 'cm', 'length_classes()', 22, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(14, 'defaults', 'global', 'Default Language', 'The default language selected, if failed to identify.', 'default_language_code', 'en', 'languages()', 10, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(15, 'defaults', 'global', 'Default Currency', 'The default currency selected, if failed to identify.', 'default_currency_code', 'USD', 'currencies()', 11, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(16, 'defaults', 'global', 'Default Country', 'The default country selected if not set otherwise.', 'default_country_code', 'IT', 'countries()', 12, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(17, 'defaults', 'global', 'Default Zone', 'The default zone selected if not set otherwise.', 'default_zone_code', '', 'zones()', 13, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(18, 'defaults', 'local', 'Default Tax Class', 'Default tax class that will be preset when creating new products.', 'default_tax_class_id', '1', 'tax_classes()', 14, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(19, 'defaults', 'global', 'Default Display Prices Including Tax', 'Displays prices including tax by default.', 'default_display_prices_including_tax', '1', 'toggle()', 15, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(20, 'defaults', 'global', 'Default Quantity Unit', 'Default quantity unit that will be preset when creating new products.', 'default_quantity_unit_id', '1', 'quantity_units()', 16, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(21, 'defaults', 'global', 'Default Sold Out Status', 'Default sold out status that will be preset when creating new products.', 'default_sold_out_status_id', '1', 'sold_out_statuses()', 17, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(22, 'defaults', 'global', 'Default Delivery Status', 'Default delivery status that will be preset when creating new products.', 'default_delivery_status_id', '1', 'delivery_statuses()', 18, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(23, 'general', 'global', 'Set Currency by Language', 'Chain select currency when changing language.', 'set_currency_by_language', '1', 'toggle()', 10, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(24, 'general', 'local', 'Contact Form CAPTCHA', 'Prevents spam by enabling CAPTCHA in the contact form.', 'contact_form_captcha_enabled', '1', 'toggle()', 11, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(25, 'general', 'global', 'Catalog Only Mode', 'Disables the cart and checkout features leaving only a browsable catalog.', 'catalog_only_mode', '0', 'toggle("t/f")', 12, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(26, 'listings', 'local', 'Items Per Page', 'The number of items to be displayed per page.', 'items_per_page', '20', 'int()', 10, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(27, 'listings', 'local', 'Data Table Rows', 'The number of data table rows per page.', 'data_table_rows_per_page', '25', 'input()', 11, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(28, 'listings', 'local', 'Display Stock Count', 'Show the available amounts of products in stock.', 'display_stock_count', '1', 'toggle()', 12, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(29, 'listings', 'local', 'Cheapest Shipping', 'Display the cheapest shipping cost on product page.', 'display_cheapest_shipping', '1', 'toggle()', 13, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(30, 'listings', 'local', 'Max Age for New Products', 'Display the new sticker for products younger than the give age. E.g. 1 month or 14 days', 'new_products_max_age', '1 month', 'input()', 14, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(31, 'listings', 'local', 'Similar Products Box: Number of Items', 'The maximum amount of items to be display in the box.', 'box_similar_products_num_items', '10', 'int()', 15, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(32, 'listings', 'local', 'Recently Viewed Products Box: Number of Items', 'The maximum amount of items to be display in the box.', 'box_recently_viewed_products_num_items', '4', 'int()', 16, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(33, 'listings', 'local', 'Latest Products Box: Number of Items', 'The maximum amount of items to be display in the box.', 'box_latest_products_num_items', '10', 'int()', 17, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(34, 'listings', 'local', 'Most Popular Products Box: Number of Items', 'The maximum amount of items to be display in the box.', 'box_most_popular_products_num_items', '10', 'int()', 18, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(35, 'listings', 'local', 'Campaign Products Box: Number of Items', 'The maximum amount of items to be display in the box.', 'box_campaign_products_num_items', '5', 'int()', 19, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(36, 'images', 'local', 'Category Images: Aspect Ratio', 'The aspect ratio of the category thumbnails', 'category_image_ratio', '16:9', 'select("1:1","2:3","3:2","3:4","4:3","16:9")', 10, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(37, 'images', 'local', 'Product Images: Aspect Ratio', 'The aspect ratio of the product thumbnails', 'product_image_ratio', '1:1', 'select("1:1","2:3","3:2","3:4","4:3","16:9")', 30, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(38, 'images', 'local', 'Product Images: Clipping Method', 'The clipping method used for scaled product thumbnails.', 'product_image_clipping', 'CROP', 'select("CROP","FIT","FIT_USE_WHITESPACING")', 31, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(39, 'images', 'local', 'Product Images: Watermark', 'Watermark product images with the store logo.', 'product_image_watermark', '0', 'toggle("y/n")', 33, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(40, 'images', 'local', 'Downsample', 'Downsample large uploaded images to best fit within the given dimensions of "width,height" or leave empty. Default: 2048,2048', 'image_downsample_size', '2048,2048', 'smallinput()', 34, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(41, 'images', 'local', 'Image Quality', 'The JPEG quality for uploaded images (0-100). Default: 90', 'image_quality', '90', 'int()', 40, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(42, 'images', 'local', 'Thumbnail Quality', 'The JPEG quality for thumbnail images (0-100). Default: 65', 'image_thumbnail_quality', '65', 'int()', 41, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(43, 'images', 'local', 'Whitespace Color', 'Set the color of any generated whitespace to the given RGB value. Default: 255,255,255', 'image_whitespace_color', '255,255,255', 'smallinput()', 42, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(44, 'checkout', 'local', 'Register Guests', 'Automatically create accounts for all guests.', 'register_guests', '0', 'toggle()', 10, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(45, 'checkout', 'local', 'Password Field', 'If Register Guests is enabled, show the password field letting customers set their own password. Otherwise randomly generated.', 'fields_customer_password', '0', 'toggle()', 11, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(46, 'checkout', 'local', 'Order Copy Receipients', 'Send order copies to the following e-mail addresses. Separate by semi-colons.', 'email_order_copy', 'store@email.com', 'mediumtext()', 12, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(47, 'checkout', 'global', 'Round Amounts', 'Round currency amounts to prevent hidden decimals.', 'round_amounts', '0', 'toggle()', 13, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(48, 'advanced', 'global', 'System Cache Enabled', 'Enables the system cache module which caches frequently used data.', 'cache_enabled', '0', 'toggle()', 10, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(49, 'advanced', 'global', 'Clear System Cache', 'Remove all cached system information.', 'cache_clear', '0', 'toggle()', 11, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(50, 'advanced', 'global', 'Clear Thumbnails Cache', 'Remove all cached image thumbnails from disk.', 'cache_clear_thumbnails', '0', 'toggle()', 12, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(51, 'advanced', 'global', 'SEO Links Language Prefix', 'Begins the SEO links with the language code i.e. /en/....', 'seo_links_language_prefix', '1', 'toggle()', 13, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(52, 'advanced', 'global', 'Regional Settings Screen', 'Enables the regional settings screen upon first visit.', 'regional_settings_screen_enabled', '0', 'toggle()', 14, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(53, 'advanced', 'global', 'Jobs Last Run', 'Time when background jobs where last executed.', 'jobs_last_run', '2015-11-15 22:10:30', 'input()', 15, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(54, 'advanced', 'global', 'GZIP Enabled', 'Compresses browser data. Increases the load on the server but decreases the bandwidth.', 'gzip_enabled', '1', 'toggle()', 16, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(55, 'advanced', 'local', 'Jobs Interval', 'The amount of minutes between each execution of jobs.', 'jobs_interval', '60', 'int()', 17, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(56, 'advanced', 'local', 'Database Admin Link', 'The URL to your database manager i.e. phpMyAdmin.', 'database_admin_link', '?app=settings&doc=advanced&action=edit&key=database_admin_link', 'input()', 18, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(57, 'advanced', 'local', 'Webmail Link', 'The URL to your webmail client.', 'webmail_link', '?app=settings&doc=advanced&action=edit&key=webmail_link', 'input()', 19, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(58, 'advanced', 'global', 'SEO Links Enabled', 'Enabling this requires .htaccess and mod_rewrite rules.', 'seo_links_enabled', '1', 'toggle()', 20, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(59, 'security', 'global', 'Blacklist', 'Deny blacklisted clients access to the site.', 'security_blacklist', '0', 'toggle("e/d")', 10, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(60, 'security', 'global', 'Session Hijacking Protection', 'Destroy sessions that were signed for a different IP address and user agent.', 'security_session_hijacking', '0', 'toggle("e/d")', 11, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(61, 'security', 'global', 'HTTP POST Protection', 'Deny incoming HTTP POST data from external sites by checking for valid form tickets.', 'security_http_post', '0', 'toggle("e/d")', 12, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(62, 'security', 'global', 'Bad Bot Trap', 'Catch bad behaving bots from crawling your website by setting up a trap.', 'security_bot_trap', '0', 'toggle("e/d")', 13, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(63, 'security', 'global', 'Cross-site Scripting (XSS) Detection', 'Detect common XSS attacks and deny access to the site.', 'security_xss', '1', 'toggle("e/d")', 14, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(64, '', 'global', 'Catalog Template', '', 'store_template_catalog', 'default.catalog', 'templates("catalog")', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(65, '', 'global', 'Admin Template', '', 'store_template_admin', 'default.admin', 'templates("admin")', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(66, '', 'global', 'Catalog Template Settings', '', 'store_template_catalog_settings', '', '', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(67, '', 'global', 'Jobs Last Push', 'Time when background jobs where last pushed for execution.', 'jobs_last_push', '2015-11-15 22:10:30', 'input()', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(68, '', 'local', 'Installed Job Modules', '', 'jobs_modules', '', '', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(69, '', 'local', 'Installed Shipping Modules', '', 'shipping_modules', '', '', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(70, '', 'local', 'Installed Payment Modules', '', 'payment_modules', '', '', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(71, '', 'local', 'Installed Order Action Modules', '', 'order_action_modules', '', '', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(72, '', 'local', 'Installed Order Total Modules', '', 'order_total_modules', 'ot_subtotal;ot_payment_fee;ot_shipping_fee', '', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(73, '', 'local', 'Installed Customer Modules', '', 'customer_modules', '', '', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(74, '', 'local', 'Installed Order Success Modules', '', 'order_success_modules', '', '', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(75, '', 'local', '', '', 'ot_payment_fee', 'a:1:{s:10:"sort_order";s:2:"10";}', '', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(76, '', 'local', '', '', 'ot_shipping_fee', 'a:1:{s:10:"sort_order";s:2:"10";}', '', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(77, '', 'local', '', '', 'ot_subtotal', 'a:1:{s:10:"sort_order";s:1:"1";}', '', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(78, '', 'local', 'Date Cache Cleared', 'Do not use system cache older than breakpoint.', 'cache_system_breakpoint', '2015-11-15 22:10:30', 'input()', 0, '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_settings_groups`
--

CREATE TABLE IF NOT EXISTS `lc_settings_groups` (
  `id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(256) NOT NULL,
  `priority` tinyint(2) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_settings_groups`
--

INSERT INTO `lc_settings_groups` (`id`, `key`, `name`, `description`, `priority`) VALUES
(1, 'store_info', 'Store Info', 'Store information', 10),
(2, 'defaults', 'Defaults', 'Store default settings', 20),
(3, 'general', 'General', 'Store default settings', 30),
(4, 'listings', 'Listings', 'Settings for the catalog listing', 40),
(5, 'images', 'Images', 'Settings for graphical elements', 50),
(6, 'checkout', 'Checkout', 'Checkout settings', 60),
(7, 'advanced', 'Advanced', 'Advanced settings', 70),
(8, 'security', 'Security', 'Site security and protection against threats', 80);

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_slides`
--

CREATE TABLE IF NOT EXISTS `lc_slides` (
  `id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `language_code` varchar(8) NOT NULL,
  `name` varchar(128) NOT NULL,
  `caption` varchar(512) NOT NULL,
  `link` varchar(256) NOT NULL,
  `image` varchar(64) NOT NULL,
  `priority` tinyint(2) NOT NULL,
  `date_valid_from` datetime NOT NULL,
  `date_valid_to` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_slides`
--

INSERT INTO `lc_slides` (`id`, `status`, `language_code`, `name`, `caption`, `link`, `image`, `priority`, `date_valid_from`, `date_valid_to`, `date_updated`, `date_created`) VALUES
(1, 1, 'en', 'Lonely Duck', '', 'http://www.canstockphoto.com/?r=282295', 'slides/1-lonely-duck.jpg', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_sold_out_statuses`
--

CREATE TABLE IF NOT EXISTS `lc_sold_out_statuses` (
  `id` int(11) NOT NULL,
  `orderable` tinyint(1) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_sold_out_statuses`
--

INSERT INTO `lc_sold_out_statuses` (`id`, `orderable`, `date_updated`, `date_created`) VALUES
(1, 1, '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(2, 1, '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_sold_out_statuses_info`
--

CREATE TABLE IF NOT EXISTS `lc_sold_out_statuses_info` (
  `id` int(11) NOT NULL,
  `sold_out_status_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(256) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_sold_out_statuses_info`
--

INSERT INTO `lc_sold_out_statuses_info` (`id`, `sold_out_status_id`, `language_code`, `name`, `description`) VALUES
(1, 1, 'en', 'Sold out', ''),
(2, 2, 'en', 'Temporary sold out', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_suppliers`
--

CREATE TABLE IF NOT EXISTS `lc_suppliers` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(24) NOT NULL,
  `link` varchar(256) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_tax_classes`
--

CREATE TABLE IF NOT EXISTS `lc_tax_classes` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(64) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_tax_rates`
--

CREATE TABLE IF NOT EXISTS `lc_tax_rates` (
  `id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `geo_zone_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(128) NOT NULL,
  `rate` decimal(10,4) NOT NULL,
  `type` enum('fixed','percent') NOT NULL DEFAULT 'percent',
  `customer_type` enum('individuals','companies','both') NOT NULL DEFAULT 'both',
  `tax_id_rule` enum('with','without','both') NOT NULL DEFAULT 'both',
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_translations`
--

CREATE TABLE IF NOT EXISTS `lc_translations` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `text_en` text NOT NULL,
  `text_de` text NOT NULL,
  `text_nb` text NOT NULL,
  `text_da` text NOT NULL,
  `text_sv` text NOT NULL,
  `html` tinyint(1) NOT NULL,
  `pages` text NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_accessed` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_translations`
--

INSERT INTO `lc_translations` (`id`, `code`, `text_en`, `text_de`, `text_nb`, `text_da`, `text_sv`, `html`, `pages`, `date_created`, `date_updated`, `date_accessed`) VALUES
(1, 'title_home', 'Home', '', '', '', '', 0, 'includes/library/lib_breadcrumbs.inc.php', '2015-11-15 22:10:40', '2015-11-15 22:10:40', '2015-11-15 22:10:51'),
(2, 'title_username', 'Username', '', '', '', '', 0, 'includes/templates/default.admin/views/login.inc.php', '2015-11-15 22:10:40', '2015-11-15 22:10:40', '2015-11-15 22:10:46'),
(3, 'title_password', 'Password', '', '', '', '', 0, 'includes/templates/default.admin/views/login.inc.php', '2015-11-15 22:10:40', '2015-11-15 22:10:40', '2015-11-15 22:10:46'),
(4, 'title_remember_me', 'Remember Me', '', '', '', '', 0, 'includes/templates/default.admin/views/login.inc.php', '2015-11-15 22:10:40', '2015-11-15 22:10:40', '2015-11-15 22:10:46'),
(5, 'title_login', 'Login', '', '', '', '', 0, 'includes/templates/default.admin/views/login.inc.php', '2015-11-15 22:10:40', '2015-11-15 22:10:40', '2015-11-15 22:10:46'),
(6, 'title_page_parse_time', 'Page Parse Time', '', '', '', '', 0, 'includes/library/lib_stats.inc.php', '2015-11-15 22:10:40', '2015-11-15 22:10:40', '2015-11-15 22:10:53'),
(7, 'title_page_capture_time', 'Page Capture Time', '', '', '', '', 0, 'includes/library/lib_stats.inc.php', '2015-11-15 22:10:40', '2015-11-15 22:10:40', '2015-11-15 22:10:53'),
(8, 'title_included_files', 'Included Files', '', '', '', '', 0, 'includes/library/lib_stats.inc.php', '2015-11-15 22:10:40', '2015-11-15 22:10:40', '2015-11-15 22:10:53'),
(9, 'title_memory_peak', 'Memory Peak', '', '', '', '', 0, 'includes/library/lib_stats.inc.php', '2015-11-15 22:10:40', '2015-11-15 22:10:40', '2015-11-15 22:10:53'),
(10, 'title_memory_limit', 'Memory Limit', '', '', '', '', 0, 'includes/library/lib_stats.inc.php', '2015-11-15 22:10:40', '2015-11-15 22:10:40', '2015-11-15 22:10:53'),
(11, 'title_database_queries', 'Database Queries', '', '', '', '', 0, 'includes/library/lib_stats.inc.php', '2015-11-15 22:10:40', '2015-11-15 22:10:40', '2015-11-15 22:10:53'),
(12, 'title_database_parse_time', 'Database Parse Time', '', '', '', '', 0, 'includes/library/lib_stats.inc.php', '2015-11-15 22:10:40', '2015-11-15 22:10:40', '2015-11-15 22:10:53'),
(13, 'error_wrong_username_password_combination', 'Wrong combination of username and password or the account does not exist.', '', '', '', '', 0, 'includes/library/lib_user.inc.php', '2015-11-15 22:10:42', '2015-11-15 22:10:42', '2015-11-15 22:10:42'),
(14, 'error_d_login_attempts_left', 'You have %d login attempts left until your account is blocked', '', '', '', '', 0, 'includes/library/lib_user.inc.php', '2015-11-15 22:10:42', '2015-11-15 22:10:42', '2015-11-15 22:10:42'),
(15, 'success_now_logged_in_as', 'You are now logged in as %username', '', '', '', '', 0, 'includes/library/lib_user.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(16, 'title_admin_panel', 'Admin Panel', '', '', '', '', 0, 'admin/index.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(17, 'title_appearence', 'Appearence', '', '', '', '', 0, 'admin/appearance.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(18, 'title_template', 'Template', '', '', '', '', 0, 'admin/appearance.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(19, 'title_logotype', 'Logotype', '', '', '', '', 0, 'admin/appearance.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(20, 'title_catalog', 'Catalog', '', '', '', '', 0, 'admin/catalog.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(21, 'title_product_groups', 'Product Groups', '', '', '', '', 0, 'admin/catalog.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(22, 'title_option_groups', 'Option Groups', '', '', '', '', 0, 'admin/catalog.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(23, 'title_manufacturers', 'Manufacturers', '', '', '', '', 0, 'admin/catalog.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(24, 'title_suppliers', 'Suppliers', '', '', '', '', 0, 'admin/catalog.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(25, 'title_delivery_statuses', 'Delivery Statuses', '', '', '', '', 0, 'admin/catalog.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(26, 'title_sold_out_statuses', 'Sold Out Statuses', '', '', '', '', 0, 'admin/catalog.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(27, 'title_quantity_units', 'Quantity Units', '', '', '', '', 0, 'admin/catalog.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(28, 'title_csv_import_export', 'CSV Import/Export', '', '', '', '', 0, 'admin/catalog.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(29, 'title_countries', 'Countries', '', '', '', '', 0, 'admin/countries.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(30, 'title_currencies', 'Currencies', '', '', '', '', 0, 'admin/currencies.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(31, 'title_customers', 'Customers', '', '', '', '', 0, 'admin/customers.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(32, 'title_newsletter', 'Newsletter', '', '', '', '', 0, 'admin/customers.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(33, 'title_geo_zones', 'Geo Zones', '', '', '', '', 0, 'admin/geo_zones.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(34, 'title_languages', 'Languages', '', '', '', '', 0, 'admin/languages.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(35, 'title_storage_encoding', 'Storage Encoding', '', '', '', '', 0, 'admin/languages.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(36, 'title_modules', 'Modules', '', '', '', '', 0, 'admin/modules.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(37, 'title_customer', 'Customer', '', '', '', '', 0, 'admin/modules.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(38, 'title_shipping', 'Shipping', '', '', '', '', 0, 'admin/modules.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(39, 'title_payment', 'Payment', '', '', '', '', 0, 'admin/modules.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(40, 'title_order_total', 'Order Total', '', '', '', '', 0, 'admin/modules.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(41, 'title_order_success', 'Order Success', '', '', '', '', 0, 'admin/modules.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(42, 'title_order_action', 'Order Action', '', '', '', '', 0, 'admin/modules.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(43, 'title_background_jobs', 'Background Jobs', '', '', '', '', 0, 'admin/modules.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(44, 'title_orders', 'Orders', '', '', '', '', 0, 'admin/orders.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(45, 'title_order_statuses', 'Order Statuses', '', '', '', '', 0, 'admin/orders.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(46, 'title_pages', 'Pages', '', '', '', '', 0, 'admin/pages.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(47, 'title_reports', 'Reports', '', '', '', '', 0, 'admin/reports.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(48, 'title_monthly_sales', 'Monthly Sales', '', '', '', '', 0, 'admin/reports.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(49, 'title_most_sold_products', 'Most Sold Products', '', '', '', '', 0, 'admin/reports.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(50, 'title_most_shopping_customers', 'Most Shopping Customers', '', '', '', '', 0, 'admin/reports.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(51, 'title_settings', 'Settings', '', '', '', '', 0, 'admin/settings.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(52, 'settings_group:title_store_info', 'Store Info', '', '', '', '', 0, 'admin/settings.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(53, 'settings_group:title_defaults', 'Defaults', '', '', '', '', 0, 'admin/settings.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(54, 'settings_group:title_general', 'General', '', '', '', '', 0, 'admin/settings.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(55, 'settings_group:title_listings', 'Listings', '', '', '', '', 0, 'admin/settings.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(56, 'settings_group:title_images', 'Images', '', '', '', '', 0, 'admin/settings.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(57, 'settings_group:title_checkout', 'Checkout', '', '', '', '', 0, 'admin/settings.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(58, 'settings_group:title_advanced', 'Advanced', '', '', '', '', 0, 'admin/settings.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(59, 'settings_group:title_security', 'Security', '', '', '', '', 0, 'admin/settings.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(60, 'title_slides', 'Slides', '', '', '', '', 0, 'admin/slides.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(61, 'title_tax', 'Tax', '', '', '', '', 0, 'admin/tax.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(62, 'title_tax_classes', 'Tax Classes', '', '', '', '', 0, 'admin/tax.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(63, 'title_tax_rates', 'Tax Rates', '', '', '', '', 0, 'admin/tax.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(64, 'title_translations', 'Translations', '', '', '', '', 0, 'admin/translations.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(65, 'title_search_translations', 'Search Translations', '', '', '', '', 0, 'admin/translations.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(66, 'title_scan_files', 'Scan Files', '', '', '', '', 0, 'admin/translations.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(67, 'title_users', 'Users', '', '', '', '', 0, 'admin/users.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(68, 'title_vqmods', 'vQmods', '', '', '', '', 0, 'admin/vqmods.app/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(69, 'warning_install_folder_exists', 'Warning: The installation directory is still available and should be deleted.', '', '', '', '', 0, 'admin/index.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(70, 'title_addons', 'Add-ons', '', '', '', '', 0, 'admin/addons.widget/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(71, 'title_discussions', 'Discussions', '', '', '', '', 0, 'admin/discussions.widget/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(72, 'title_sales', 'Sales', '', '', '', '', 0, 'admin/sales.widget/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(73, 'title_statistics', 'Statistics', '', '', '', '', 0, 'admin/stats.widget/config.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(74, 'title_s_months', '%s months', '', '', '', '', 0, 'admin/sales.widget/sales.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(75, 'title_s_days', '%s days', '', '', '', '', 0, 'admin/sales.widget/sales.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(76, 'title_total_sales', 'Total Sales', '', '', '', '', 0, 'admin/stats.widget/stats.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(77, 'title_average_order_amount', 'Average Order Amount', '', '', '', '', 0, 'admin/stats.widget/stats.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(78, 'title_number_of_customers', 'Number of Customers', '', '', '', '', 0, 'admin/stats.widget/stats.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(79, 'title_number_of_products', 'Number of Products', '', '', '', '', 0, 'admin/stats.widget/stats.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(80, 'title_id', 'ID', '', '', '', '', 0, 'admin/orders.widget/orders.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(81, 'title_customer_name', 'Customer Name', '', '', '', '', 0, 'admin/orders.widget/orders.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(82, 'title_order_status', 'Order Status', '', '', '', '', 0, 'admin/orders.widget/orders.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(83, 'title_amount', 'Amount', '', '', '', '', 0, 'admin/orders.widget/orders.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(84, 'title_date', 'Date', '', '', '', '', 0, 'admin/orders.widget/orders.inc.php', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:51'),
(85, 'title_latest_addons', 'Latest Add-ons', '', '', '', '', 0, 'admin/addons.widget/addons.inc.php', '2015-11-15 22:10:53', '2015-11-15 22:10:53', '2015-11-15 22:10:53'),
(86, 'title_most_recent_forum_topics', 'Most Recent Forum Topics', '', '', '', '', 0, 'admin/discussions.widget/discussions.inc.php', '2015-11-15 22:10:53', '2015-11-15 22:10:53', '2015-11-15 22:10:53'),
(87, 'text_by', 'by', '', '', '', '', 0, 'admin/discussions.widget/discussions.inc.php', '2015-11-15 22:10:53', '2015-11-15 22:10:53', '2015-11-15 22:10:53'),
(88, 'title_webmail', 'Webmail', '', '', '', '', 0, 'includes/templates/default.admin/layouts/default.inc.php', '2015-11-15 22:10:53', '2015-11-15 22:10:53', '2015-11-15 22:10:53'),
(89, 'title_database_manager', 'Database Manager', '', '', '', '', 0, 'includes/templates/default.admin/layouts/default.inc.php', '2015-11-15 22:10:53', '2015-11-15 22:10:53', '2015-11-15 22:10:53'),
(90, 'text_logout', 'Logout', '', '', '', '', 0, 'includes/templates/default.admin/layouts/default.inc.php', '2015-11-15 22:10:53', '2015-11-15 22:10:53', '2015-11-15 22:10:53');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_users`
--

CREATE TABLE IF NOT EXISTS `lc_users` (
  `id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_ip` varchar(15) NOT NULL,
  `last_host` varchar(64) NOT NULL,
  `login_attempts` int(11) NOT NULL,
  `total_logins` int(11) NOT NULL,
  `date_blocked` datetime NOT NULL,
  `date_expires` datetime NOT NULL,
  `date_active` datetime NOT NULL,
  `date_login` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_users`
--

INSERT INTO `lc_users` (`id`, `status`, `username`, `password`, `last_ip`, `last_host`, `login_attempts`, `total_logins`, `date_blocked`, `date_expires`, `date_active`, `date_login`, `date_created`, `date_updated`) VALUES
(1, 1, 'admin', 'ec2c441237651c106b4fba75699b3855ac494c791b567655fa577619fc428c0e', '127.0.0.1', 'localhost', 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-15 22:10:51', '2015-11-15 22:10:51', '2015-11-15 22:10:30', '2015-11-15 22:10:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_zones`
--

CREATE TABLE IF NOT EXISTS `lc_zones` (
  `id` int(11) NOT NULL,
  `country_code` varchar(4) NOT NULL,
  `code` varchar(8) NOT NULL,
  `name` varchar(64) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_zones`
--

INSERT INTO `lc_zones` (`id`, `country_code`, `code`, `name`, `date_updated`, `date_created`) VALUES
(1, 'CA', 'AB', 'Alberta', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'CA', 'BC', 'British Columbia', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'CA', 'MB', 'Manitoba', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'CA', 'NB', 'New Brunswick', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'CA', 'NL', 'Newfoundland and Labrador', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'CA', 'NT', 'Northwest Territories', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'CA', 'NS', 'Nova Scotia', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'CA', 'NU', 'Nunavut', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'CA', 'ON', 'Ontario', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'CA', 'PE', 'Prince Edward Island', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'CA', 'QC', 'Québec', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'CA', 'SK', 'Saskatchewan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'CA', 'YT', 'Yukon Territory', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'US', 'AL', 'Alabama', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'US', 'AK', 'Alaska', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'US', 'AS', 'American Samoa', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'US', 'AZ', 'Arizona', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'US', 'AR', 'Arkansas', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'US', 'AF', 'Armed Forces Africa', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'US', 'AA', 'Armed Forces Americas', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'US', 'AC', 'Armed Forces Canada', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'US', 'AE', 'Armed Forces Europe', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'US', 'AM', 'Armed Forces Middle East', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'US', 'AP', 'Armed Forces Pacific', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'US', 'CA', 'California', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'US', 'CO', 'Colorado', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'US', 'CT', 'Connecticut', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'US', 'DE', 'Delaware', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'US', 'DC', 'District of Columbia', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'US', 'FM', 'Federated States Of Micronesia', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'US', 'FL', 'Florida', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'US', 'GA', 'Georgia', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'US', 'GU', 'Guam', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'US', 'HI', 'Hawaii', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'US', 'ID', 'Idaho', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'US', 'IL', 'Illinois', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'US', 'IN', 'Indiana', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'US', 'IA', 'Iowa', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'US', 'KS', 'Kansas', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'US', 'KY', 'Kentucky', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'US', 'LA', 'Louisiana', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 'US', 'ME', 'Maine', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 'US', 'MH', 'Marshall Islands', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'US', 'MD', 'Maryland', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 'US', 'MA', 'Massachusetts', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 'US', 'MI', 'Michigan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 'US', 'MN', 'Minnesota', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 'US', 'MS', 'Mississippi', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 'US', 'MO', 'Missouri', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 'US', 'MT', 'Montana', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 'US', 'NE', 'Nebraska', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 'US', 'NV', 'Nevada', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 'US', 'NH', 'New Hampshire', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 'US', 'NJ', 'New Jersey', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 'US', 'NM', 'New Mexico', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 'US', 'NY', 'New York', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 'US', 'NC', 'North Carolina', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 'US', 'ND', 'North Dakota', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 'US', 'MP', 'Northern Mariana Islands', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 'US', 'OH', 'Ohio', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 'US', 'OK', 'Oklahoma', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 'US', 'OR', 'Oregon', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 'US', 'PW', 'Palau', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 'US', 'PA', 'Pennsylvania', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 'US', 'PR', 'Puerto Rico', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 'US', 'RI', 'Rhode Island', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 'US', 'SC', 'South Carolina', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 'US', 'SD', 'South Dakota', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 'US', 'TN', 'Tennessee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 'US', 'TX', 'Texas', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 'US', 'UT', 'Utah', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 'US', 'VT', 'Vermont', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 'US', 'VI', 'Virgin Islands', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 'US', 'VA', 'Virginia', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 'US', 'WA', 'Washington', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 'US', 'WV', 'West Virginia', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 'US', 'WI', 'Wisconsin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 'US', 'WY', 'Wyoming', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struttura della tabella `lc_zones_to_geo_zones`
--

CREATE TABLE IF NOT EXISTS `lc_zones_to_geo_zones` (
  `id` int(11) NOT NULL,
  `geo_zone_id` int(11) NOT NULL,
  `country_code` varchar(2) NOT NULL,
  `zone_code` varchar(8) NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lc_zones_to_geo_zones`
--

INSERT INTO `lc_zones_to_geo_zones` (`id`, `geo_zone_id`, `country_code`, `zone_code`, `date_updated`, `date_created`) VALUES
(1, 1, 'AT', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(2, 1, 'BE', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(3, 1, 'BG', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(4, 1, 'CY', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(5, 1, 'CZ', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(6, 1, 'DE', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(7, 1, 'DK', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(8, 1, 'EE', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(9, 1, 'ES', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(10, 1, 'FR', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(11, 1, 'FI', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(12, 1, 'GB', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(13, 1, 'GR', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(14, 1, 'HR', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(15, 1, 'HU', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(16, 1, 'IE', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(17, 1, 'IT', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(18, 1, 'LT', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(19, 1, 'LU', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(20, 1, 'LV', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(21, 1, 'MT', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(22, 1, 'NL', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(23, 1, 'PL', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(24, 1, 'PT', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(25, 1, 'RO', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(26, 1, 'SE', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(27, 1, 'SI', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(28, 1, 'SK', '', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(29, 2, 'CA', 'AB', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(30, 2, 'CA', 'BC', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(31, 2, 'CA', 'ON', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(32, 2, 'CA', 'QC', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(33, 2, 'CA', 'NS', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(34, 2, 'CA', 'NB', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(35, 2, 'CA', 'MB', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(36, 2, 'CA', 'PE', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(37, 2, 'CA', 'SK', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(38, 2, 'CA', 'NL', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(39, 3, 'US', 'AL', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(40, 3, 'US', 'AK', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(41, 3, 'US', 'AZ', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(42, 3, 'US', 'AR', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(43, 3, 'US', 'CA', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(44, 3, 'US', 'CO', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(45, 3, 'US', 'CT', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(46, 3, 'US', 'DE', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(47, 3, 'US', 'FL', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(48, 3, 'US', 'GA', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(49, 3, 'US', 'HI', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(50, 3, 'US', 'ID', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(51, 3, 'US', 'IL', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(52, 3, 'US', 'IN', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(53, 3, 'US', 'IA', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(54, 3, 'US', 'KS', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(55, 3, 'US', 'KY', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(56, 3, 'US', 'LA', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(57, 3, 'US', 'ME', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(58, 3, 'US', 'MD', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(59, 3, 'US', 'MA', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(60, 3, 'US', 'MI', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(61, 3, 'US', 'MN', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(62, 3, 'US', 'MS', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(63, 3, 'US', 'MO', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(64, 3, 'US', 'MT', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(65, 3, 'US', 'NE', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(66, 3, 'US', 'NV', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(67, 3, 'US', 'NH', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(68, 3, 'US', 'NJ', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(69, 3, 'US', 'NM', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(70, 3, 'US', 'NY', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(71, 3, 'US', 'NC', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(72, 3, 'US', 'ND', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(73, 3, 'US', 'OH', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(74, 3, 'US', 'OK', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(75, 3, 'US', 'OR', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(76, 3, 'US', 'PA', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(77, 3, 'US', 'RI', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(78, 3, 'US', 'SC', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(79, 3, 'US', 'SD', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(80, 3, 'US', 'TN', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(81, 3, 'US', 'TX', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(82, 3, 'US', 'UT', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(83, 3, 'US', 'VT', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(84, 3, 'US', 'VA', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(85, 3, 'US', 'WA', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(86, 3, 'US', 'WV', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(87, 3, 'US', 'WI', '2015-11-15 22:10:30', '2015-11-15 22:10:30'),
(88, 3, 'US', 'WY', '2015-11-15 22:10:30', '2015-11-15 22:10:30');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `lc_addresses`
--
ALTER TABLE `lc_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indici per le tabelle `lc_cart_items`
--
ALTER TABLE `lc_cart_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `cart_uid` (`cart_uid`);

--
-- Indici per le tabelle `lc_categories`
--
ALTER TABLE `lc_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `code` (`code`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `status` (`status`),
  ADD KEY `dock` (`dock`);

--
-- Indici per le tabelle `lc_categories_info`
--
ALTER TABLE `lc_categories_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `language_code` (`language_code`);

--
-- Indici per le tabelle `lc_countries`
--
ALTER TABLE `lc_countries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `iso_code_2` (`iso_code_2`),
  ADD UNIQUE KEY `iso_code_3` (`iso_code_3`),
  ADD KEY `status` (`status`);

--
-- Indici per le tabelle `lc_currencies`
--
ALTER TABLE `lc_currencies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`);

--
-- Indici per le tabelle `lc_customers`
--
ALTER TABLE `lc_customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indici per le tabelle `lc_delivery_statuses`
--
ALTER TABLE `lc_delivery_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `lc_delivery_statuses_info`
--
ALTER TABLE `lc_delivery_statuses_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_status_id` (`delivery_status_id`),
  ADD KEY `language_code` (`language_code`);

--
-- Indici per le tabelle `lc_geo_zones`
--
ALTER TABLE `lc_geo_zones`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `lc_languages`
--
ALTER TABLE `lc_languages`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `status` (`status`);

--
-- Indici per le tabelle `lc_manufacturers`
--
ALTER TABLE `lc_manufacturers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `code` (`code`),
  ADD KEY `status` (`status`);

--
-- Indici per le tabelle `lc_manufacturers_info`
--
ALTER TABLE `lc_manufacturers_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `manufacturer_id` (`manufacturer_id`),
  ADD KEY `language_code` (`language_code`);

--
-- Indici per le tabelle `lc_option_groups`
--
ALTER TABLE `lc_option_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `lc_option_groups_info`
--
ALTER TABLE `lc_option_groups_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `language_code` (`language_code`);

--
-- Indici per le tabelle `lc_option_values`
--
ALTER TABLE `lc_option_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indici per le tabelle `lc_option_values_info`
--
ALTER TABLE `lc_option_values_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `value_id` (`value_id`),
  ADD KEY `language_code` (`language_code`);

--
-- Indici per le tabelle `lc_orders`
--
ALTER TABLE `lc_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_status_id` (`order_status_id`);

--
-- Indici per le tabelle `lc_orders_comments`
--
ALTER TABLE `lc_orders_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indici per le tabelle `lc_orders_items`
--
ALTER TABLE `lc_orders_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indici per le tabelle `lc_orders_totals`
--
ALTER TABLE `lc_orders_totals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indici per le tabelle `lc_order_statuses`
--
ALTER TABLE `lc_order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `lc_order_statuses_info`
--
ALTER TABLE `lc_order_statuses_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_status_id` (`order_status_id`),
  ADD KEY `language_code` (`language_code`);

--
-- Indici per le tabelle `lc_pages`
--
ALTER TABLE `lc_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `dock` (`dock`);

--
-- Indici per le tabelle `lc_pages_info`
--
ALTER TABLE `lc_pages_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_id` (`page_id`),
  ADD KEY `language_code` (`language_code`);

--
-- Indici per le tabelle `lc_products`
--
ALTER TABLE `lc_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `default_category_id` (`default_category_id`),
  ADD KEY `manufacturer_id` (`manufacturer_id`),
  ADD KEY `keywords` (`keywords`),
  ADD KEY `code` (`code`),
  ADD KEY `date_valid_from` (`date_valid_from`),
  ADD KEY `date_valid_to` (`date_valid_to`),
  ADD KEY `purchases` (`purchases`),
  ADD KEY `views` (`views`),
  ADD KEY `product_groups` (`product_groups`);

--
-- Indici per le tabelle `lc_products_campaigns`
--
ALTER TABLE `lc_products_campaigns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indici per le tabelle `lc_products_images`
--
ALTER TABLE `lc_products_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indici per le tabelle `lc_products_info`
--
ALTER TABLE `lc_products_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `language_code` (`language_code`);

--
-- Indici per le tabelle `lc_products_options`
--
ALTER TABLE `lc_products_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indici per le tabelle `lc_products_options_stock`
--
ALTER TABLE `lc_products_options_stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indici per le tabelle `lc_products_prices`
--
ALTER TABLE `lc_products_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indici per le tabelle `lc_products_to_categories`
--
ALTER TABLE `lc_products_to_categories`
  ADD PRIMARY KEY (`product_id`,`category_id`);

--
-- Indici per le tabelle `lc_product_groups`
--
ALTER TABLE `lc_product_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`);

--
-- Indici per le tabelle `lc_product_groups_info`
--
ALTER TABLE `lc_product_groups_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_group_id` (`product_group_id`),
  ADD KEY `language_code` (`language_code`);

--
-- Indici per le tabelle `lc_product_groups_values`
--
ALTER TABLE `lc_product_groups_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_group_id` (`product_group_id`);

--
-- Indici per le tabelle `lc_product_groups_values_info`
--
ALTER TABLE `lc_product_groups_values_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_group_value_id` (`product_group_value_id`),
  ADD KEY `language_code` (`language_code`);

--
-- Indici per le tabelle `lc_quantity_units`
--
ALTER TABLE `lc_quantity_units`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `lc_quantity_units_info`
--
ALTER TABLE `lc_quantity_units_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quantity_unit_id` (`quantity_unit_id`),
  ADD KEY `language_code` (`language_code`);

--
-- Indici per le tabelle `lc_settings`
--
ALTER TABLE `lc_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`),
  ADD KEY `setting_group_key` (`setting_group_key`);

--
-- Indici per le tabelle `lc_settings_groups`
--
ALTER TABLE `lc_settings_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Indici per le tabelle `lc_slides`
--
ALTER TABLE `lc_slides`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `lc_sold_out_statuses`
--
ALTER TABLE `lc_sold_out_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `lc_sold_out_statuses_info`
--
ALTER TABLE `lc_sold_out_statuses_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sold_out_status_id` (`sold_out_status_id`),
  ADD KEY `language_code` (`language_code`);

--
-- Indici per le tabelle `lc_suppliers`
--
ALTER TABLE `lc_suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `lc_tax_classes`
--
ALTER TABLE `lc_tax_classes`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `lc_tax_rates`
--
ALTER TABLE `lc_tax_rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tax_class_id` (`tax_class_id`),
  ADD KEY `geo_zone_id` (`geo_zone_id`);

--
-- Indici per le tabelle `lc_translations`
--
ALTER TABLE `lc_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indici per le tabelle `lc_users`
--
ALTER TABLE `lc_users`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `lc_zones`
--
ALTER TABLE `lc_zones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_code` (`country_code`);

--
-- Indici per le tabelle `lc_zones_to_geo_zones`
--
ALTER TABLE `lc_zones_to_geo_zones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `geo_zone_id` (`geo_zone_id`),
  ADD KEY `country_code` (`country_code`),
  ADD KEY `zone_code` (`zone_code`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `lc_addresses`
--
ALTER TABLE `lc_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `lc_cart_items`
--
ALTER TABLE `lc_cart_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `lc_categories`
--
ALTER TABLE `lc_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la tabella `lc_categories_info`
--
ALTER TABLE `lc_categories_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la tabella `lc_countries`
--
ALTER TABLE `lc_countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=240;
--
-- AUTO_INCREMENT per la tabella `lc_currencies`
--
ALTER TABLE `lc_currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la tabella `lc_customers`
--
ALTER TABLE `lc_customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `lc_delivery_statuses`
--
ALTER TABLE `lc_delivery_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `lc_delivery_statuses_info`
--
ALTER TABLE `lc_delivery_statuses_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `lc_geo_zones`
--
ALTER TABLE `lc_geo_zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT per la tabella `lc_languages`
--
ALTER TABLE `lc_languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `lc_manufacturers`
--
ALTER TABLE `lc_manufacturers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `lc_manufacturers_info`
--
ALTER TABLE `lc_manufacturers_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `lc_option_groups`
--
ALTER TABLE `lc_option_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `lc_option_groups_info`
--
ALTER TABLE `lc_option_groups_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `lc_option_values`
--
ALTER TABLE `lc_option_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT per la tabella `lc_option_values_info`
--
ALTER TABLE `lc_option_values_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT per la tabella `lc_orders`
--
ALTER TABLE `lc_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `lc_orders_comments`
--
ALTER TABLE `lc_orders_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `lc_orders_items`
--
ALTER TABLE `lc_orders_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `lc_orders_totals`
--
ALTER TABLE `lc_orders_totals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `lc_order_statuses`
--
ALTER TABLE `lc_order_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT per la tabella `lc_order_statuses_info`
--
ALTER TABLE `lc_order_statuses_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT per la tabella `lc_pages`
--
ALTER TABLE `lc_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT per la tabella `lc_pages_info`
--
ALTER TABLE `lc_pages_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT per la tabella `lc_products`
--
ALTER TABLE `lc_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT per la tabella `lc_products_campaigns`
--
ALTER TABLE `lc_products_campaigns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `lc_products_images`
--
ALTER TABLE `lc_products_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT per la tabella `lc_products_info`
--
ALTER TABLE `lc_products_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT per la tabella `lc_products_options`
--
ALTER TABLE `lc_products_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT per la tabella `lc_products_options_stock`
--
ALTER TABLE `lc_products_options_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT per la tabella `lc_products_prices`
--
ALTER TABLE `lc_products_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT per la tabella `lc_product_groups`
--
ALTER TABLE `lc_product_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `lc_product_groups_info`
--
ALTER TABLE `lc_product_groups_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `lc_product_groups_values`
--
ALTER TABLE `lc_product_groups_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT per la tabella `lc_product_groups_values_info`
--
ALTER TABLE `lc_product_groups_values_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT per la tabella `lc_quantity_units`
--
ALTER TABLE `lc_quantity_units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `lc_quantity_units_info`
--
ALTER TABLE `lc_quantity_units_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `lc_settings`
--
ALTER TABLE `lc_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT per la tabella `lc_settings_groups`
--
ALTER TABLE `lc_settings_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT per la tabella `lc_slides`
--
ALTER TABLE `lc_slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `lc_sold_out_statuses`
--
ALTER TABLE `lc_sold_out_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la tabella `lc_sold_out_statuses_info`
--
ALTER TABLE `lc_sold_out_statuses_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la tabella `lc_suppliers`
--
ALTER TABLE `lc_suppliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `lc_tax_classes`
--
ALTER TABLE `lc_tax_classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `lc_tax_rates`
--
ALTER TABLE `lc_tax_rates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `lc_translations`
--
ALTER TABLE `lc_translations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT per la tabella `lc_users`
--
ALTER TABLE `lc_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `lc_zones`
--
ALTER TABLE `lc_zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT per la tabella `lc_zones_to_geo_zones`
--
ALTER TABLE `lc_zones_to_geo_zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=89;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
