################
# Instructions #
################

1. Always backup data before making changes to your store.
2. Upload the contents of the folder public_html/* to the corresponding path of your LiteCart installation.


#########################
# WHERE UPLOAD THE FILE #
#########################

<ul>
  <li>public_html/
    <ul>
      <li>includes/
          <ul>
            <li>templates/
              <ul>
                  <li>default.catalog/
                    <ul>
                      <li>layouts/
                        <ul>
                          <li> default.inc.php</li>
                          </ul>
                      </li>
                    </ul>
                  </li>
              </ul>
            </li>
          </ul>
      </li>
    </ul>
  </li>
</ul>

#########
# Done! #
#########
